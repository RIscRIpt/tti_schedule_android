package com.riscript.ttischedule;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.Schedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ScheduleAdapter extends DateRecyclerViewAdapter<ScheduleAdapter.ViewHolder> {

    static class ViewHolder extends RecyclerView.ViewHolder {
        final LinearLayout dayView;
        final VerticalTextView dow;
        final VerticalTextView date;
        final TextView statusText;
        final ExpandedListView list;
        final ScheduleClassAdapter listAdapter;

        ViewHolder(LinearLayout dayView) {
            super(dayView);

            this.dayView = dayView;
            this.dow = (VerticalTextView)dayView.findViewById(R.id.sd_dow);
            this.date = (VerticalTextView)dayView.findViewById(R.id.sd_date);
            this.statusText = (TextView)dayView.findViewById(R.id.sd_status);
            this.list = (ExpandedListView)dayView.findViewById(R.id.sd_list);
            this.listAdapter = new ScheduleClassAdapter(dayView.getContext());

            list.setAdapter(listAdapter);

            list.setLongClickable(true);
        }
    }

    private final Fragment parent;
    private final Schedule schedule;
    private final String[] dow2str;
    private final String[] dow2string;
    private final String txtNoLessons;
    private final SimpleDateFormat datefmt_dayMonth;

    public ScheduleAdapter(Fragment parent, Schedule schedule, long baseDayMillis) {
        super(parent.getContext(), baseDayMillis);

        this.parent = parent;
        this.schedule = schedule;

        Resources resources = parent.getContext().getResources();
        dow2str = resources.getStringArray(R.array.dow2str);
        dow2string = resources.getStringArray(R.array.dow2string);

        txtNoLessons = resources.getString(R.string.no_lessons);

        datefmt_dayMonth = new SimpleDateFormat(resources.getString(R.string.datefmt_day_month), Locale.US);
    }

    @Override
    public ScheduleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout scheduleDayView = (LinearLayout)LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.schedule_day, parent, false);

        scheduleDayView.setLongClickable(true);

        ViewHolder viewHolder = new ViewHolder(scheduleDayView);
        this.parent.registerForContextMenu(viewHolder.list);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Calendar day) {
        Database.Class[] classesOfDay = schedule.getClassesOfDay(day);

        if(classesOfDay.length > 0) {
            holder.statusText.setVisibility(View.GONE);
            //holder.list.setAdapter(new ScheduleClassAdapter(holder.dayView.getContext(), classesOfDay));
            holder.listAdapter.setClasses(classesOfDay);
            holder.list.setAdapter(holder.listAdapter);
            holder.list.setVisibility(View.VISIBLE);
        } else {
            holder.list.setVisibility(View.GONE);
            holder.statusText.setText(txtNoLessons);
            holder.statusText.setVisibility(View.VISIBLE);
        }

        holder.date.setText(datefmt_dayMonth.format(day.getTime()));

        String[] d2s;
        if(classesOfDay.length > 1) {
            d2s = dow2string;
        } else {
            d2s = dow2str;
        }

        holder.dow.setText(d2s[day.get(Calendar.DAY_OF_WEEK) - 1]);
        if((day.get(Calendar.DAY_OF_YEAR) & 1) == 0) {
            holder.dow.setBackgroundColor(colorEven.data);
            holder.date.setBackgroundColor(colorEven.data);
        } else {
            holder.dow.setBackgroundColor(colorOdd.data);
            holder.date.setBackgroundColor(colorOdd.data);
        }
    }

    @Override
    public int getItemCount() {
        return DateLayoutManager.ITEM_COUNT;
    }
}
