package com.riscript.ttischedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SettingsCacheFragment extends SettingsFragment implements ScheduleProvider.ClearedListener {

    private Button btnClearCache;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.initialize();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_cache, container, false);

        btnClearCache = (Button)view.findViewById(R.id.btn_clear_cache);
        btnClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCache();
            }
        });

        return view;
    }

    void clearCache() {
        ScheduleProvider.getInstance()
                .requestClear(this);

        btnClearCache.setEnabled(false);
    }

    @Override
    public void onCleared() {
        btnClearCache.setEnabled(true);

        if(getContext() != null) {
            Toast.makeText(getContext(), R.string.cache_is_clean, Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
