package com.riscript.ttischedule;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.DiskDatabase;
import com.riscript.ttischedule.database.Symbols;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SettingsEditFilterFragment
        extends SettingsFragment
        implements
        ScheduleProvider.ResponseSymbolsListener,
        ScheduleProvider.ErrorListener,
        ScheduleProvider.ResponseFilterListener
{
    public static final int EDIT_FILTER_NEW = -1;

    private Database.Filter editFilter;

    private View form;

    private AutoCompleteTextView groupTextView;
    private AutoCompleteTextView teacherTextView;
    private AutoCompleteTextView roomTextView;

    private TextView statusTextView;

    private ProgressBar refetchProgressBar;
    private Button saveButton;

    private MenuItem menuItem_refetch;

    public SettingsEditFilterFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.initialize();

        RelativeLayout layout = (RelativeLayout)inflater.inflate(R.layout.settings_edit_filter, container, false);

        form = layout.findViewById(R.id.sef_form);
        groupTextView = (AutoCompleteTextView)layout.findViewById(R.id.sef_group);
        teacherTextView = (AutoCompleteTextView)layout.findViewById(R.id.sef_teacher);
        roomTextView = (AutoCompleteTextView)layout.findViewById(R.id.sef_room);
        statusTextView = (TextView)layout.findViewById(R.id.sef_status);

        refetchProgressBar = (ProgressBar)layout.findViewById(R.id.sef_refetch_pb);
        saveButton = (Button)layout.findViewById(R.id.sef_save);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editFilter != null) {
                    editFilter.save();
                }
                SettingsActivity settingsActivity = (SettingsActivity)SettingsEditFilterFragment.this.getActivity();
                settingsActivity.navigateBack();
            }
        });

        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_settings_edit_filter, menu);

        menuItem_refetch = menu.findItem(R.id.action_refetch_symbols);
    }

    @Override
    public void onStart() {
        super.onStart();

        form.setEnabled(false);
        saveButton.setEnabled(false);
        groupTextView.setText(null);
        teacherTextView.setText(null);
        roomTextView.setText(null);

        reloadSuggestions(false);

        ScheduleProvider.getInstance()
                .requestFilter(this, getParam());
    }

    @Override
    public void onPause() {
        super.onPause();

        ScheduleProvider.getInstance()
                .cancelAllRequests(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refetch_symbols:
                reloadSuggestions(true);
                break;
            default:
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setStatusText(String text) {
        if(statusTextView != null) {
            if(text != null && text.length() > 0) {
                statusTextView.setText(text);
                statusTextView.setVisibility(View.VISIBLE);
            } else {
                statusTextView.setText(null);
                statusTextView.setVisibility(View.GONE);
            }
        }
    }

    private void reloadSuggestions(boolean refetch) {
        setStatusText(getString(R.string.status_refetching_suggestions));

        form.setEnabled(false);
        if(menuItem_refetch != null)
            menuItem_refetch.setEnabled(false);
        refetchProgressBar.setVisibility(View.VISIBLE);

        ScheduleProvider.getInstance()
                .requestSymbols(this, refetch);
    }

    @Override
    public void onSymbolsRetrieved(Symbols symbols) {
        if(!isAdded())
            return;

        setSuggestions(groupTextView, new ArrayList<>(symbols.groups));
        setSuggestions(teacherTextView, new ArrayList<>(symbols.teachers));
        setSuggestions(roomTextView, new ArrayList<>(symbols.rooms));

        onAllResponsesCompleted();
    }

    private void setSuggestions(AutoCompleteTextView autoCompTxtView, List<String> suggestions) {
        if(autoCompTxtView == null)
            return;
        Collections.sort(suggestions);
        autoCompTxtView.setAdapter(
                new ArrayAdapter<String>(getContext(), R.layout.suggestion_item, suggestions)
        );
    }

    private void onAllResponsesCompleted() {
        form.setEnabled(true);
        if(menuItem_refetch != null)
            menuItem_refetch.setEnabled(true);
        refetchProgressBar.setVisibility(View.GONE);
        setStatusText(null);
    }

    @Override
    public void onError(String message) {
        if(!isAdded())
            return;

        onAllResponsesCompleted();

        setStatusText(message);
    }

    @Override
    public void onFilterReceived(Database.Filter filter) {
        if(!isAdded())
            return;

        if(editFilter == filter)
            return;

        editFilter = filter;

        form.setEnabled(true);
        saveButton.setEnabled(editFilter.isValid());
        groupTextView.setText(editFilter.getGroup());
        teacherTextView.setText(editFilter.getTeacher());
        roomTextView.setText(editFilter.getRoom());

        groupTextView.addTextChangedListener(
                new SyncedTextChangeListener(groupTextView, editFilter.getGroupRefValue())
        );
        teacherTextView.addTextChangedListener(
                new SyncedTextChangeListener(teacherTextView, editFilter.getTeacherRefValue())
        );
        roomTextView.addTextChangedListener(
                new SyncedTextChangeListener(roomTextView, editFilter.getRoomRefValue())
        );
    }

    private class SyncedTextChangeListener implements TextWatcher {
        private final EditText editText;
        private final DiskDatabase.RefValue refValue;
        private final String stringNotFound;

        SyncedTextChangeListener(EditText editText, DiskDatabase.RefValue refValue) {
            this.editText = editText;
            this.refValue = refValue;

            stringNotFound = getString(R.string.not_found);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            String value = s.toString();
            if(value.length() == 0)
                value = null;
            refValue.setValue(value);
            if(refValue.isValid() || refValue.isEmpty()) {
                editText.setError(null);
            } else {
                editText.setError(stringNotFound);
            }
            saveButton.setEnabled(editFilter.isValid());
        }
    }
}
