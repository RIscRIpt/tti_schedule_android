package com.riscript.ttischedule;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import static android.support.v7.widget.RecyclerView.NO_POSITION;

public class ScheduleScrollSyncer {
    private class ListScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            syncDaysToList(dy);
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            switch(newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    syncDaysToList(0);
            }
        }
    }

    private class DaysScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            syncListToDays(dx);
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            switch(newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    syncListToDays(0);
            }
        }
    }

    private boolean synchronizing;

    private RecyclerView list;
    private RecyclerView days;

    private ListScrollListener listOnScrollListener;
    private DaysScrollListener daysOnScrollListener;

    ScheduleScrollSyncer() {
        synchronizing = false;
        listOnScrollListener = new ListScrollListener();
        daysOnScrollListener = new DaysScrollListener();
    }

    public RecyclerView getList() {
        return list;
    }

    public RecyclerView getDays() {
        return days;
    }

    public RecyclerView setList(RecyclerView newList) {
        RecyclerView oldList = list;
        list = newList;

        if(oldList != null) {
            oldList.removeOnScrollListener(listOnScrollListener);
        }
        if(newList != null) {
            newList.addOnScrollListener(listOnScrollListener);
        }

        if(days != null) {
            syncListToDays(0);
        }

        return oldList;
    }

    public RecyclerView setDays(RecyclerView newDays) {
        RecyclerView oldDays = days;
        days = newDays;

        if(oldDays != null) {
            oldDays.removeOnScrollListener(daysOnScrollListener);
        }
        if(newDays != null) {
            newDays.addOnScrollListener(daysOnScrollListener);
        }

        if(list != null) {
            syncDaysToList(0);
        }

        return oldDays;
    }

    public void syncDaysToList(int scrollOffset) {
        if(synchronizing)
            return;

        synchronizing = true;

        final DateLayoutManager scheduleLayoutManager =
                (DateLayoutManager)getList().getLayoutManager();

        final int firstVisible = scheduleLayoutManager.findFirstVisibleItemPosition();
        if(firstVisible == NO_POSITION) {
            synchronizing = false;
            return;
        }

        final RecyclerView days = getDays();
        if(days == null) {
            synchronizing = false;
            return;
        }

        final DateLayoutManager daysScrollerLayoutManager =
                (DateLayoutManager)days.getLayoutManager();

        final View scheduleView = scheduleLayoutManager.findViewByPosition(firstVisible);
        if(scheduleView == null) {
            synchronizing = false;
            return;
        }

        View dayView = daysScrollerLayoutManager.findViewByPosition(firstVisible);
        if(dayView == null) {
            if(scrollOffset == 0)
                daysScrollerLayoutManager.scrollToPositionWithOffset(firstVisible, 0);
            else
                days.scrollBy(scrollOffset, 0);
        } else {
            final int offset = (int)(scheduleView.getY() / (float)scheduleView.getHeight() * dayView.getWidth());
            daysScrollerLayoutManager.scrollToPositionWithOffset(firstVisible, offset);
        }

        synchronizing = false;
    }

    public void syncListToDays(int scrollOffset) {
        if(synchronizing)
            return;

        synchronizing = true;

        final DateLayoutManager daysScrollerLayoutManager =
                (DateLayoutManager)getDays().getLayoutManager();

        int firstVisible = daysScrollerLayoutManager.findFirstVisibleItemPosition();
        if(firstVisible == NO_POSITION) {
            synchronizing = false;
            return;
        }

        RecyclerView list = getList();
        if(list == null) {
            synchronizing = false;
            return;
        }

        final DateLayoutManager scheduleLayoutManager =
                (DateLayoutManager)list.getLayoutManager();

        final View dayView = daysScrollerLayoutManager.findViewByPosition(firstVisible);
        if(dayView == null) {
            synchronizing = false;
            return;
        }

        View scheduleView = scheduleLayoutManager.findViewByPosition(firstVisible);
        if(scheduleView == null) {
            if(scrollOffset == 0)
                scheduleLayoutManager.scrollToPositionWithOffset(firstVisible, 0);
            else
                list.scrollBy(0, scrollOffset);
        } else {
            final int offset = (int)(dayView.getX() / (float)dayView.getWidth() * (float)scheduleView.getHeight());
            scheduleLayoutManager.scrollToPositionWithOffset(firstVisible, offset);
        }

        synchronizing = false;
    }
}
