package com.riscript.ttischedule.database;

import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

public class Schedule {
    private final Database.Filter filter;
    private final TreeMap<Long, Database.Class> classes;

    Schedule(Database.Filter filter, TreeMap<Long, Database.Class> classes) {
        this.filter = filter;
        this.classes = classes;
    }

    public String getTitle() {
        if(filter == null)
            return null;
        return filter.getTitle();
    }

    public Database.Filter getFilter() {
        return filter;
    }

    // Time of `day` must be set to 00:00 AM
    public Database.Class[] getClassesOfDay(Calendar day) {
        Map.Entry<Long, Database.Class> entry = classes.ceilingEntry(day.getTimeInMillis());
        ArrayList<Database.Class> dayClasses = new ArrayList<>(0);
        while(entry != null && sameDay(entry.getValue().time, day)) {
            dayClasses.add(entry.getValue());
            entry = classes.higherEntry(entry.getKey());
        }
        return dayClasses.toArray(new Database.Class[0]);
    }

    private boolean sameDay(Calendar day1, Calendar day2) {
        return day1.get(Calendar.DAY_OF_YEAR) == day2.get(Calendar.DAY_OF_YEAR) &&
                day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR);
    }
}
