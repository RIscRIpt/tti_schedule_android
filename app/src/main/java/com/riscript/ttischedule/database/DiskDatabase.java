package com.riscript.ttischedule.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.TreeMap;

import static android.database.Cursor.FIELD_TYPE_INTEGER;
import static android.database.Cursor.FIELD_TYPE_STRING;
import static com.riscript.ttischedule.database.Database.INVALID_ID;

public class DiskDatabase {
    private final DiskDbHelper dbHelper;
    final SQLiteDatabase database;
    final RefValueFactory refValueFactory;

    DiskDatabase(Context context) {
        dbHelper = new DiskDbHelper(context);
        database = dbHelper.getWritableDatabase();

        refValueFactory = new RefValueFactory();
    }

    private void beginTransaction() {
        database.beginTransactionNonExclusive();
    }
    private void endTransaction() {
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    private int clearTable(String tableName) {
        return database.delete(tableName, null, null);
    }
    void clearTableTeachers() {
        clearTable(DiskDbContract.Teacher.TABLE_NAME);

        // Create null entry
        ContentValues cv = new ContentValues();
        cv.put(DiskDbContract.Teacher._ID, 0);
        cv.put(DiskDbContract.Teacher.NAME, "");
        insert(DiskDbContract.Teacher.TABLE_NAME, cv);

    }
    void clearTableGroups() {
        clearTable(DiskDbContract.Group.TABLE_NAME);

        // Create null entry
        ContentValues cv = new ContentValues();
        cv.put(DiskDbContract.Group._ID, 0);
        cv.put(DiskDbContract.Group.NAME, "");
        insert(DiskDbContract.Group.TABLE_NAME, cv);
    }
    void clearTableRooms() {
        clearTable(DiskDbContract.Room.TABLE_NAME);

        // Create null entry
        ContentValues cv = new ContentValues();
        cv.put(DiskDbContract.Room._ID, 0);
        cv.put(DiskDbContract.Room.NAME, "");
        insert(DiskDbContract.Room.TABLE_NAME, cv);
    }

    public void clear() {
        dbHelper.onUpgrade(database, -1, 0);
    }

    boolean tableIsEmpty(String table) {
        SQLiteStatement s = database.compileStatement(DiskDbContract.getCountQuery(table));
        int minEntryCount = 1;
        if(table == DiskDbContract.Group.TABLE_NAME
                || table == DiskDbContract.Teacher.TABLE_NAME
                || table == DiskDbContract.Class.TABLE_NAME)
        {
            minEntryCount = 2;
        }
        return s.simpleQueryForLong() < minEntryCount; //ignore fake null entries
    }

    public boolean hasSymbols() {
        return !tableIsEmpty(DiskDbContract.Group.TABLE_NAME)
                && !tableIsEmpty(DiskDbContract.Teacher.TABLE_NAME)
                && !tableIsEmpty(DiskDbContract.Room.TABLE_NAME);
    }

    private Hashtable<Long, String> getKeyValues(String tableName, String idName, String valueName) {
        Cursor cursor = database.query(tableName, null, null, null, null, null, null);
        Hashtable<Long, String> table = new Hashtable<>(cursor.getCount());
        int col_id = cursor.getColumnIndex(idName);
        int col_value = cursor.getColumnIndex(valueName);
        while(cursor.moveToNext()) {
            table.put(cursor.getLong(col_id), cursor.getString(col_value));
        }
        cursor.close();
        return table;
    }
    Hashtable<Long, String> getTeachers() {
        return getKeyValues(
                DiskDbContract.Teacher.TABLE_NAME,
                DiskDbContract.Teacher._ID,
                DiskDbContract.Teacher.NAME
        );
    }
    Hashtable<Long, String> getGroups() {
        return getKeyValues(
                DiskDbContract.Group.TABLE_NAME,
                DiskDbContract.Group._ID,
                DiskDbContract.Group.NAME
        );
    }
    Hashtable<Long, String> getRooms() {
        return getKeyValues(
                DiskDbContract.Room.TABLE_NAME,
                DiskDbContract.Room._ID,
                DiskDbContract.Room.NAME
        );
    }

    TreeMap<Long, Database.Class> getSchedule(Filter filter) {
        long gid = filter.group.getId();
        long tid = filter.teacher.getId();
        long rid = filter.room.getId();

        Cursor cursor = database.rawQuery(DiskDbContract.getScheduleQuery(gid, tid, rid), null);

        int col_timestamp = cursor.getColumnIndex(DiskDbContract.Schedule.TIMESTAMP);
        int col_name = cursor.getColumnIndex(DiskDbContract.Schedule.NAME);
        int col_teacher = cursor.getColumnIndex(DiskDbContract.Schedule.TEACHER);
        int col_comment = cursor.getColumnIndex(DiskDbContract.Schedule.COMMENT);
        int col_status = cursor.getColumnIndex(DiskDbContract.Schedule.STATUS);
        int col_groups = cursor.getColumnIndex(DiskDbContract.Schedule.GROUPS);
        int col_rooms = cursor.getColumnIndex(DiskDbContract.Schedule.ROOMS);

        TreeMap<Long, Database.Class> timetable = new TreeMap<>();

        while(cursor.moveToNext()) {
            long timestamp = cursor.getLong(col_timestamp);
            String name = cursor.getString(col_name);
            String comment = cursor.getString(col_comment);
            String status = cursor.getString(col_status);

            String teacher = null;
            if(filter.teacher.isEmpty())
                teacher = cursor.getString(col_teacher);

            String mergedGroups = null;
            String mergedRooms = null;
            if(filter.group.isEmpty())
                mergedGroups = cursor.getString(col_groups);
            if(filter.room.isEmpty())
                mergedRooms = cursor.getString(col_rooms);

            String[] groups = null;
            String[] rooms = null;
            if(mergedGroups != null)
                groups = mergedGroups.split(",");
            if(mergedRooms != null)
                rooms = mergedRooms.split(",");

            Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            time.setTimeInMillis(timestamp);
            timetable.put(time.getTimeInMillis(), new Database.Class(
                    time,
                    name,
                    teacher,
                    comment,
                    status,
                    groups,
                    rooms
            ));
        }

        cursor.close();

        return timetable;
    }

    private int delete(String table, String where, String[] args) {
        return database.delete(table, where, args);
    }

    private long update(String table, ContentValues values, String where, String[] args) {
        // This database is only a cache for online data,
        // so on conflict replace with the new data
        return database.updateWithOnConflict(table, values, where, args, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private long insert(String table, ContentValues values) {
        // This database is only a cache for online data,
        // so on conflict replace with the new data
        return database.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    abstract class Inserter {
        protected final ContentValues cv;
        private final String tableName;
        private final String[] columns;

        protected Inserter(String tableName, String... columns) {
            this.tableName = tableName;
            this.columns = columns;

            this.cv = new ContentValues(columns.length);
        }

        protected long insert() {
            return DiskDatabase.this.insert(tableName, cv);
        }

        void begin() {
            DiskDatabase.this.beginTransaction();
        }

        void end() {
            DiskDatabase.this.endTransaction();
        }
    }
    class TeacherInserter extends Inserter {
        TeacherInserter() {
            super(
                    DiskDbContract.Teacher.TABLE_NAME,
                    DiskDbContract.Teacher._ID,
                    DiskDbContract.Teacher.NAME
            );
        }
        long insert(long id, String name) {
            cv.put(DiskDbContract.Teacher._ID, id);
            cv.put(DiskDbContract.Teacher.NAME, name);
            return super.insert();
        }
    }
    class GroupInserter extends Inserter {
        GroupInserter() {
            super(
                    DiskDbContract.Group.TABLE_NAME,
                    DiskDbContract.Group._ID,
                    DiskDbContract.Group.NAME
            );
        }
        long insert(long id, String name) {
            cv.put(DiskDbContract.Group._ID, id);
            cv.put(DiskDbContract.Group.NAME, name);
            return super.insert();
        }
    }
    class RoomInserter extends Inserter {
        RoomInserter() {
            super(
                    DiskDbContract.Room.TABLE_NAME,
                    DiskDbContract.Room._ID,
                    DiskDbContract.Room.NAME
            );
        }
        long insert(long id, String name) {
            cv.put(DiskDbContract.Room._ID, id);
            cv.put(DiskDbContract.Room.NAME, name);
            return super.insert();
        }
    }
    TeacherInserter getTeacherInserter() {
        return new TeacherInserter();
    }
    GroupInserter getGroupInserter() {
        return new GroupInserter();
    }
    RoomInserter getRoomInserter() {
        return new RoomInserter();
    }

    long updateClass(long timestamp, long[] rooms, long[] groups, long teacherId, long classNameId, long commentId, long statusId) {
        ContentValues cv = new ContentValues();

        Cursor queryId = database.query(DiskDbContract.Class.TABLE_NAME, new String[]{DiskDbContract.Class._ID},
                DiskDbContract.Class.TIMESTAMP + "=? AND " +
                        DiskDbContract.Class.TEACHER_ID + "=? AND " +
                        DiskDbContract.Class.NAME_ID + "=?",
                new String[]{
                        String.valueOf(timestamp),
                        String.valueOf(teacherId),
                        String.valueOf(classNameId)
                },
                null, null, null);

        if(queryId.moveToNext()) {
            cv.put(DiskDbContract.Class._ID, queryId.getLong(0));
        }
        cv.put(DiskDbContract.Class.TIMESTAMP, timestamp);
        cv.put(DiskDbContract.Class.TEACHER_ID, teacherId);
        cv.put(DiskDbContract.Class.NAME_ID, classNameId);
        cv.put(DiskDbContract.Class.COMMENT_ID, commentId);
        cv.put(DiskDbContract.Class.STATUS_ID, statusId);

        long classId = insert(DiskDbContract.Class.TABLE_NAME, cv);
        if(classId == INVALID_ID)
            return INVALID_ID;

        cv.clear();
        cv.put(DiskDbContract.ClassRooms.CLASS_ID, classId);
        for(int i = 0; i < rooms.length; i++) {
            cv.put(DiskDbContract.ClassRooms.ROOM_ID, rooms[i]);
            insert(DiskDbContract.ClassRooms.TABLE_NAME, cv);
        }

        cv.clear();
        cv.put(DiskDbContract.ClassGroups.CLASS_ID, classId);
        for(int i = 0; i < groups.length; i++) {
            cv.put(DiskDbContract.ClassGroups.GROUP_ID, groups[i]);
            insert(DiskDbContract.ClassGroups.TABLE_NAME, cv);
        }

        return classId;
    }

    public class RefValue<T> {
        private final String tableName;
        private final String idName;
        private final String valueName;

        private long id;
        private Object value;

        RefValue(String tableName, String idName, String valueName) {
            this(tableName, idName, valueName, INVALID_ID, null);
        }

        RefValue(String tableName, String idName, String valueName, long id, T value) {
            this.tableName = tableName;
            this.idName = idName;
            this.valueName = valueName;

            this.id = id;
            this.value = value;
        }

        public boolean isEmpty() {
            return id == INVALID_ID && value == null;
        }

        public long getId() {
            if(!compile())
                return INVALID_ID;
            return id;
        }

        public T getValue() {
            if(!compile())
                return null;
            return (T)value;
        }

        public void setValue(T newValue) {
            id = INVALID_ID;
            value = newValue;
        }

        public boolean isValid() {
            return compile();
        }

        public void setId(long newId) {
            id = newId;
            value = null;
        }

        boolean compile() {
            if(id == INVALID_ID && value != null) {
                Cursor cursor = database.query(
                        tableName,
                        new String[]{idName},
                        valueName + " = ?",
                        new String[]{value.toString()},
                        null,
                        null,
                        null
                );
                if(cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    id = cursor.getLong(0);
                }
                cursor.close();
            }

            if(value == null && id != INVALID_ID) {
                Cursor cursor = database.query(
                        tableName,
                        new String[]{valueName},
                        idName + " = ?",
                        new String[]{Long.toString(id)},
                        null,
                        null,
                        null
                );
                if(cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    cursor.getType(0);
                    switch(cursor.getType(0)) {
                        case FIELD_TYPE_INTEGER:
                            value = cursor.getLong(0);
                            break;
                        case FIELD_TYPE_STRING:
                            value = cursor.getString(0);
                            break;
                        default:
                            throw new TypeNotPresentException(String.format("Cannot make synced type %d", cursor.getType(0)), null);
                    }
                }
                cursor.close();
            }

            return id != INVALID_ID && value != null;
        }
    }
    class RefValueFactory {
        RefValue fromGroupTable() {
            return fromGroupTable(null);
        }

        RefValue fromGroupTable(String value) {
            return fromGroupTable(INVALID_ID, value);
        }

        RefValue fromGroupTable(long id, String value) {
            return new RefValue(DiskDbContract.Group.TABLE_NAME, DiskDbContract.Group._ID, DiskDbContract.Group.NAME, id, value);
        }

        RefValue fromTeacherTable() {
            return fromTeacherTable(null);
        }

        RefValue fromTeacherTable(String value) {
            return fromTeacherTable(INVALID_ID, value);
        }

        RefValue fromTeacherTable(long id, String value) {
            return new RefValue(DiskDbContract.Teacher.TABLE_NAME, DiskDbContract.Teacher._ID, DiskDbContract.Teacher.NAME, id, value);
        }

        RefValue fromRoomTable() {
            return fromRoomTable(null);
        }

        RefValue fromRoomTable(String value) {
            return fromRoomTable(INVALID_ID, value);
        }

        RefValue fromRoomTable(long id, String value) {
            return new RefValue(DiskDbContract.Room.TABLE_NAME, DiskDbContract.Room._ID, DiskDbContract.Room.NAME, id, value);
        }
    }

    class Filter {
        protected long id;
        protected long order;

        protected final RefValue<String> group;
        protected final RefValue<String> teacher;
        protected final RefValue<String> room;

        protected boolean has_schedule;

        Filter(long id, long order, long groupId, long teacherId, long roomId, boolean has_schedule) {
            this(
                    id,
                    order,
                    refValueFactory.fromGroupTable(groupId, null),
                    refValueFactory.fromTeacherTable(teacherId, null),
                    refValueFactory.fromRoomTable(roomId, null),
                    has_schedule
            );
        }

        Filter(long id, long order, String group, String teacher, String room, boolean has_schedule) {
            this(
                    id,
                    order,
                    refValueFactory.fromGroupTable(group),
                    refValueFactory.fromTeacherTable(teacher),
                    refValueFactory.fromRoomTable(room),
                    has_schedule
            );
        }

        Filter(long id, long order, RefValue group, RefValue teacher, RefValue room, boolean has_schedule) {
            this.id = id;
            this.order = order;
            this.group = group;
            this.teacher = teacher;
            this.room = room;
            this.has_schedule = has_schedule;
        }

        boolean save() {
            long gid = group.getId();
            long tid = teacher.getId();
            long rid = room.getId();

            ContentValues cv = new ContentValues(4);
            cv.put(DiskDbContract.Filter.ORDER, order);
            cv.put(DiskDbContract.Filter.GROUP_ID, gid);
            cv.put(DiskDbContract.Filter.TEACHER_ID, tid);
            cv.put(DiskDbContract.Filter.ROOM_ID, rid);
            if(cv.size() == 0)
                return false;
            if(id != INVALID_ID) cv.put(DiskDbContract.Filter._ID, id);
            long result = insert(DiskDbContract.Filter.TABLE_NAME, cv);
            if(id == INVALID_ID)
                id = result;
            return result != 0;
        }

        boolean setHasSchedule(boolean has) {
            if(id == INVALID_ID)
                return false;

            ContentValues cv = new ContentValues(1);
            cv.put(DiskDbContract.Filter.HAS_SCHEDULE, has ? 1 : 0);
            long result = update(DiskDbContract.Filter.TABLE_NAME, cv, DiskDbContract.Filter._ID + " = ?", new String[] { Long.toString(id) });
            if(result != 0)
                has_schedule = has;
            return result != 0;
        }

        public void destroy() {
            if(id == INVALID_ID)
                return;

            delete(DiskDbContract.Filter.TABLE_NAME, DiskDbContract.Filter._ID + " = ?", new String[] { Long.toString(id) });
        }

        public boolean setOrder(int order) {
            if(id == INVALID_ID)
                return false;

            ContentValues cv = new ContentValues(1);
            cv.put(DiskDbContract.Filter.ORDER, order);
            long result = update(DiskDbContract.Filter.TABLE_NAME, cv, DiskDbContract.Filter._ID + " = ?", new String[] { Long.toString(id) });
            if(result != 0)
                this.order = order;
            return result != 0;
        }
    }

    Filter makeSyncedFilter() {
        return new Filter(INVALID_ID, Long.MAX_VALUE, INVALID_ID, INVALID_ID, INVALID_ID, false);
    }

    Filter makeSyncedFilter(String group, String teacher, String room) {
        return new Filter(INVALID_ID, Long.MAX_VALUE, group, teacher, room, false);
    }

    Hashtable<Long, Filter> getFilters() {
        Hashtable<Long, Filter> filters = new Hashtable();

        Cursor cursor = database.query(DiskDbContract.Filter.TABLE_NAME, null, null, null, null, null, null);
        int col_id = cursor.getColumnIndexOrThrow(DiskDbContract.Filter._ID);
        int col_order = cursor.getColumnIndexOrThrow(DiskDbContract.Filter.ORDER);
        int col_group_id = cursor.getColumnIndexOrThrow(DiskDbContract.Filter.GROUP_ID);
        int col_teacher_id = cursor.getColumnIndexOrThrow(DiskDbContract.Filter.TEACHER_ID);
        int col_room_id = cursor.getColumnIndexOrThrow(DiskDbContract.Filter.ROOM_ID);
        int col_has_schedule_id = cursor.getColumnIndexOrThrow(DiskDbContract.Filter.HAS_SCHEDULE);

        while(cursor.moveToNext()) {
            long id = cursor.getLong(col_id);
            long order = cursor.getLong(col_order);
            long group_id = cursor.getLong(col_group_id);
            long teacher_id = cursor.getLong(col_teacher_id);
            long room_id = cursor.getLong(col_room_id);
            long has_schedule = cursor.getLong(col_has_schedule_id);
            filters.put(id, new Filter(id, order, group_id, teacher_id, room_id, has_schedule != 0));
        }

        cursor.close();

        return filters;
    }

    //region retrieving id (getting id of existent entry, or adding a new one and returning its id).
    private long retrieveId(String tableName, String idName, String valueName, String value) {
        long id = INVALID_ID;
        Cursor cursor = database.query(
                tableName,
                new String[]{idName},
                valueName + " = ?",
                new String[]{value},
                null,
                null,
                null
        );
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getLong(0);
        }
        cursor.close();
        if(id == INVALID_ID) {
            ContentValues cv = new ContentValues();
            cv.put(valueName, value);
            id = insert(tableName, cv);
        }
        return id;
    }

    long retrieveClassNameId(String name) {
        return retrieveId(
                DiskDbContract.ClassName.TABLE_NAME,
                DiskDbContract.ClassName._ID,
                DiskDbContract.ClassName.NAME,
                name
        );
    }

    long retrieveCommentId(String name) {
        return retrieveId(
                DiskDbContract.Comment.TABLE_NAME,
                DiskDbContract.Comment._ID,
                DiskDbContract.Comment.TEXT,
                name
        );
    }

    long retrieveStatusId(String name) {
        return retrieveId(
                DiskDbContract.Status.TABLE_NAME,
                DiskDbContract.Status._ID,
                DiskDbContract.Status.STATUS,
                name
        );
    }
    //endregion

}
