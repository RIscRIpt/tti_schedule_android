package com.riscript.ttischedule.database;

import com.riscript.ttischedule.FiltersAdapter;
import com.riscript.ttischedule.ReorderableArrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.riscript.ttischedule.database.Database.INVALID_ID;

public class Filters {
    public interface DataSetChangeListener {
        void onDataSetChange();
        void onMove(int fromPosition, int toPosition);
    }

    private final ArrayList<DataSetChangeListener> dataSetChangeListeners;
    private final Database database;
    private final Hashtable<Long, DiskDatabase.Filter> filters;
    private final ReorderableArrayList<DiskDatabase.Filter> orderedFilters;

    Filters(Database database, Hashtable<Long, DiskDatabase.Filter> filters) {
        this.database = database;
        this.filters = filters;

        orderedFilters = new ReorderableArrayList<DiskDatabase.Filter>(filters.values()) {
            @Override
            public void onPositionChanged(DiskDatabase.Filter element, int newPosition) {
                element.setOrder(newPosition);
            }
        };
        dataSetChangeListeners = new ArrayList<>();

        Collections.sort(orderedFilters, new Comparator<DiskDatabase.Filter>() {
            @Override
            public int compare(DiskDatabase.Filter filter1, DiskDatabase.Filter filter2) {
                return (int)(filter1.order - filter2.order);
            }
        });
    }

    public int size() {
        return filters.size();
    }

    public Database.Filter getById(long index) {
        return database.makeFilter(filters.get(index));
    }

    public Database.Filter getByOrder(int order) {
        return database.makeFilter(orderedFilters.get(order));
    }

    void add(DiskDatabase.Filter filter) {
        if(filter.id == INVALID_ID)
            return;

        filters.put(filter.id, filter);
        orderedFilters.add(filter);

        onDataSetChange();
    }

    public void removeByOrder(int position) {
        final DiskDatabase.Filter filter = orderedFilters.get(position);
        filters.remove(filter.id);
        orderedFilters.remove(position);
        filter.destroy();
        onDataSetChange();
    }

    public void move(int fromPosition, int toPosition) {
        orderedFilters.move(fromPosition, toPosition);
        onMove(fromPosition, toPosition);
    }

    private void onDataSetChange() {
        for(DataSetChangeListener listener : new HashSet<>(dataSetChangeListeners)) {
            listener.onDataSetChange();
            //todo: check if listener is actual
            // or unsubscribe in other place
        }
    }

    private void onMove(int fromPosition, int toPosition) {
        for(DataSetChangeListener listener : new HashSet<>(dataSetChangeListeners)) {
            listener.onMove(fromPosition, toPosition);
            //todo: check if listener is actual
            // or unsubscribe in other place
        }
    }

    public void subscribeDataSetChange(DataSetChangeListener changeListener) {
        dataSetChangeListeners.add(changeListener);
    }

    public void unsubscribeDataSetChange(DataSetChangeListener changeListener) {
        dataSetChangeListeners.remove(changeListener);
    }

    public void clear() {
        filters.clear();
        orderedFilters.clear();
    }
}
