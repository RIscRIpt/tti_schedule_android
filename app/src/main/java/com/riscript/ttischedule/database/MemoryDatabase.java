package com.riscript.ttischedule.database;

import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

import static com.riscript.ttischedule.database.Database.INVALID_ID;

class MemoryDatabase {

    private Hashtable<Long, String> teachers;
    private Hashtable<Long, String> groups;
    private Hashtable<Long, String> rooms;

    private Hashtable<String, Long> classNameIds;
    private Hashtable<String, Long> commentIds;
    private Hashtable<String, Long> statusIds;

    MemoryDatabase() {
        classNameIds = new Hashtable<>();
        commentIds = new Hashtable<>();
        statusIds = new Hashtable<>();

        classNameIds = new Hashtable<>();
        commentIds = new Hashtable<>();
        statusIds = new Hashtable<>();
    }

    boolean hasSymbols() {
        return teachers != null && !teachers.isEmpty()
                && groups != null && !groups.isEmpty()
                && rooms != null && !rooms.isEmpty();
    }
    Collection<String> getTeachers() {
        if(teachers == null)
            return null;
        return teachers.values();
    }
    Collection<String> getGroups() {
        if(groups == null)
            return null;
        return groups.values();
    }
    Collection<String> getRooms() {
        if(rooms == null)
            return null;
        return rooms.values();
    }

    void setTeachers(Hashtable<Long, String> teachers) {
        this.teachers = teachers;
    }
    void setGroups(Hashtable<Long, String> groups) {
        this.groups = groups;
    }
    void setRooms(Hashtable<Long, String> rooms) {
        this.rooms = rooms;
    }

    public long getClassNameId(String name) {
        Long id = classNameIds.get(name);
        if(id == null)
            return INVALID_ID;
        return id;
    }

    public void setClassNameId(String name, long id) {
        classNameIds.put(name, id);
    }

    public long getCommentId(String comment) {
        Long id = commentIds.get(comment);
        if(id == null)
            return INVALID_ID;
        return id;
    }

    public void setCommentId(String comment, long id) {
        commentIds.put(comment, id);
    }

    public long getStatusId(String status) {
        Long id = statusIds.get(status);
        if(id == null)
            return INVALID_ID;
        return id;
    }

    public void setStatusId(String status, long id) {
        statusIds.put(status, id);
    }

    public void clear() {
        teachers = null;
        groups = null;
        rooms = null;

        classNameIds.clear();
        commentIds.clear();
        statusIds.clear();
    }
}
