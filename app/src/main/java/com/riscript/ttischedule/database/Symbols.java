package com.riscript.ttischedule.database;

import java.util.Collection;

public class Symbols {
    public final Collection<String> groups;
    public final Collection<String> teachers;
    public final Collection<String> rooms;

    public Symbols(Collection<String> groups, Collection<String> teachers, Collection<String> rooms) {
        this.groups = groups;
        this.teachers = teachers;
        this.rooms = rooms;
    }
}
