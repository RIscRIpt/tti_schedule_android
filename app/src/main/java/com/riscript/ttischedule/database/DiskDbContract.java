package com.riscript.ttischedule.database;

import android.os.StrictMode;
import android.provider.BaseColumns;

import static com.riscript.ttischedule.database.Database.INVALID_ID;

public class DiskDbContract {
    private DiskDbContract() {
    }

    public static class Class implements BaseColumns {
        public static final String TABLE_NAME = "c_class";
        public static final String TIMESTAMP = "c_timestamp";
        public static final String NAME_ID = "c_name_id";
        public static final String TEACHER_ID = "c_teacher_id";
        public static final String COMMENT_ID = "c_comment_id";
        public static final String STATUS_ID = "c_status_id";
    }

    public static class ClassName implements BaseColumns {
        public static final String TABLE_NAME = "c_classname";
        public static final String NAME = "c_name";
    }

    public static class Teacher implements BaseColumns {
        public static final String TABLE_NAME = "c_teacher";
        public static final String NAME = "c_name";
    }

    public static class Comment implements BaseColumns {
        public static final String TABLE_NAME = "c_comment";
        public static final String TEXT = "c_text";
    }

    public static class Status implements BaseColumns {
        public static final String TABLE_NAME = "c_status";
        public static final String STATUS = "c_status";
    }

    public static final String SQL_CREATE_CLASS =
            "CREATE TABLE " + Class.TABLE_NAME + " (" +
                    Class._ID        + " " + "INTEGER" + " " + "PRIMARY KEY AUTOINCREMENT" + "," +
                    Class.TIMESTAMP  + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    Class.NAME_ID    + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    Class.TEACHER_ID + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    Class.COMMENT_ID + " " + "INTEGER" + " " + "NULL"     + "," +
                    Class.STATUS_ID  + " " + "INTEGER" + " " + "NULL"     + "," +
                    "UNIQUE (" + Class.TIMESTAMP + "," + Class.NAME_ID + "," + Class.TEACHER_ID + ") ON CONFLICT IGNORE" + "," +
                    "FOREIGN KEY (" + Class.NAME_ID    + ") REFERENCES " + ClassName.TABLE_NAME + "(" + ClassName._ID + ")" + "," +
                    "FOREIGN KEY (" + Class.TEACHER_ID + ") REFERENCES " + Teacher.TABLE_NAME   + "(" + Teacher._ID   + ")" + "," +
                    "FOREIGN KEY (" + Class.COMMENT_ID + ") REFERENCES " + Comment.TABLE_NAME   + "(" + Comment._ID   + ")" + "," +
                    "FOREIGN KEY (" + Class.STATUS_ID  + ") REFERENCES " + Status.TABLE_NAME    + "(" + Status._ID    + ")" + ");" ;

    public static final String SQL_CREATE_CLASSNAME =
            "CREATE TABLE " + ClassName.TABLE_NAME + " (" +
                    ClassName._ID  + " " + "INTEGER" + " " + "PRIMARY KEY" + "," +
                    ClassName.NAME + " " + "TEXT"    + " " + "NOT NULL" + ");";

    public static final String SQL_CREATE_TEACHER =
            "CREATE TABLE " + Teacher.TABLE_NAME + " (" +
                    Teacher._ID  + " " + "INTEGER" + " " + "PRIMARY KEY" + "," +
                    Teacher.NAME + " " + "TEXT"    + " " + "NOT NULL" + ");";

    public static final String SQL_CREATE_COMMENT =
            "CREATE TABLE " + Comment.TABLE_NAME + " (" +
                    Comment._ID  + " " + "INTEGER" + " " + "PRIMARY KEY AUTOINCREMENT" + "," +
                    Comment.TEXT + " " + "TEXT"    + " " + "NOT NULL" + ");";

    public static final String SQL_CREATE_STATUS =
            "CREATE TABLE " + Status.TABLE_NAME + " (" +
                    Status._ID    + " " + "INTEGER" + " " + "PRIMARY KEY AUTOINCREMENT" + "," +
                    Status.STATUS + " " + "TEXT"    + " " + "NOT NULL" + ");";


    public static class Group implements BaseColumns {
        public static final String TABLE_NAME = "c_group";
        public static final String NAME = "c_name";
    }

    public static final String SQL_CREATE_GROUP =
            "CREATE TABLE " + Group.TABLE_NAME + " (" +
                    Group._ID  + " " + "INTEGER" + " " + "PRIMARY KEY" + "," +
                    Group.NAME + " " + "TEXT"    + " " + "NOT NULL" + ");";

    // _ID is not needed
    public static class ClassGroups /*implements BaseColumns*/ {
        public static final String TABLE_NAME = "c_class_groups";
        public static final String CLASS_ID = "c_class_id";
        public static final String GROUP_ID = "c_group_id";
    }

    public static final String SQL_CREATE_CLASSGROUPS =
            "CREATE TABLE " + ClassGroups.TABLE_NAME + " (" +
                    ClassGroups.CLASS_ID + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    ClassGroups.GROUP_ID + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    "PRIMARY KEY (" + ClassGroups.CLASS_ID + "," + ClassGroups.GROUP_ID + ")" + "," +
                    "FOREIGN KEY (" + ClassGroups.CLASS_ID + ") REFERENCES " + Class.TABLE_NAME + "(" + Class._ID + ")" + "," +
                    "FOREIGN KEY (" + ClassGroups.GROUP_ID + ") REFERENCES " + Group.TABLE_NAME + "(" + Group._ID + ")" + ");";

    public static class Room implements BaseColumns {
        public static final String TABLE_NAME = "c_room";
        public static final String NAME = "c_name";
    }

    public static final String SQL_CREATE_ROOM =
            "CREATE TABLE " + Room.TABLE_NAME + " (" +
                    Room._ID  + " " + "INTEGER" + " " + "PRIMARY KEY" + "," +
                    Room.NAME + " " + "TEXT"    + " " + "NOT NULL" + ");";

    // _ID is not needed
    public static class ClassRooms /*implements BaseColumns*/ {
        public static final String TABLE_NAME = "c_class_rooms";
        public static final String CLASS_ID = "c_class_id";
        public static final String ROOM_ID = "c_room_id";
    }

    public static final String SQL_CREATE_CLASSROOMS =
            "CREATE TABLE " + ClassRooms.TABLE_NAME + " (" +
                    ClassRooms.CLASS_ID + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    ClassRooms.ROOM_ID  + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    "PRIMARY KEY (" + ClassRooms.CLASS_ID + "," + ClassRooms.ROOM_ID + ")" + "," +
                    "FOREIGN KEY (" + ClassRooms.CLASS_ID + ") REFERENCES " + Class.TABLE_NAME + "(" + Class._ID + ")" + "," +
                    "FOREIGN KEY (" + ClassRooms.ROOM_ID  + ") REFERENCES " + Room.TABLE_NAME  + "(" + Room._ID  + ")" + ");";

    public static class Schedule implements BaseColumns {
        public static final String VIEW_NAME = "c_schedule";
        public static final String TIMESTAMP = "c_timestamp";
        public static final String NAME = "c_name";
        public static final String TEACHER = "c_teacher";
        public static final String COMMENT = "c_comment";
        public static final String STATUS = "c_status";
        public static final String GROUPS = "c_groups";
        public static final String ROOMS = "c_rooms";
    }

    public static class Filter implements BaseColumns {
        public static final String TABLE_NAME = "c_filters";
        public static final String ORDER = "c_order";
        public static final String GROUP_ID = "c_group_id";
        public static final String TEACHER_ID = "c_teacher_id";
        public static final String ROOM_ID = "c_room_id";
        public static final String HAS_SCHEDULE = "c_has_schedule";
    }

    public static final String SQL_CREATE_FILTER =
            "CREATE TABLE " + Filter.TABLE_NAME + " (" +
                    Filter._ID          + " " + "INTEGER" + " " + "PRIMARY KEY AUTOINCREMENT" + "," +
                    Filter.ORDER        + " " + "INTEGER" + " " + "NOT NULL" + "," +
                    Filter.GROUP_ID     + " " + "INTEGER" + " " + "NOT NULL DEFAULT " + INVALID_ID + "," +
                    Filter.TEACHER_ID   + " " + "INTEGER" + " " + "NOT NULL DEFAULT " + INVALID_ID + "," +
                    Filter.ROOM_ID      + " " + "INTEGER" + " " + "NOT NULL DEFAULT " + INVALID_ID + "," +
                    Filter.HAS_SCHEDULE + " " + "INTEGER" + " " + "NOT NULL DEFAULT " + INVALID_ID + "," +
                    "FOREIGN KEY (" + Filter.GROUP_ID   + ") REFERENCES " + Group.TABLE_NAME   + "(" + Group._ID   + ")" + "," +
                    "FOREIGN KEY (" + Filter.TEACHER_ID + ") REFERENCES " + Teacher.TABLE_NAME + "(" + Teacher._ID + ")" + "," +
                    "FOREIGN KEY (" + Filter.GROUP_ID   + ") REFERENCES " + Room.TABLE_NAME    + "(" + Room._ID    + ")" + ");";


    public static final String SQL_DELETE_CLASS =
            "DROP TABLE IF EXISTS " + Class.TABLE_NAME;

    public static final String SQL_DELETE_CLASSNAME =
            "DROP TABLE IF EXISTS " + ClassName.TABLE_NAME;

    public static final String SQL_DELETE_TEACHER =
            "DROP TABLE IF EXISTS " + Teacher.TABLE_NAME;

    public static final String SQL_DELETE_COMMENT =
            "DROP TABLE IF EXISTS " + Comment.TABLE_NAME;

    public static final String SQL_DELETE_STATUS =
            "DROP TABLE IF EXISTS " + Status.TABLE_NAME;

    public static final String SQL_DELETE_GROUP =
            "DROP TABLE IF EXISTS " + Group.TABLE_NAME;

    public static final String SQL_DELETE_CLASSGROUPS =
            "DROP TABLE IF EXISTS " + ClassGroups.TABLE_NAME;

    public static final String SQL_DELETE_ROOM =
            "DROP TABLE IF EXISTS " + Room.TABLE_NAME;

    public static final String SQL_DELETE_CLASSROOMS =
            "DROP TABLE IF EXISTS " + ClassRooms.TABLE_NAME;

    public static final String SQL_DELETE_FILTER =
            "DROP TABLE IF EXISTS " + Filter.TABLE_NAME;


    public static String getCountQuery(String table) {
        return "SELECT COUNT(*) FROM " + table;
    }


    public static String getScheduleQuery(long groupId, long teacherId, long roomId) {
        String selection = "";

        if(groupId != INVALID_ID) {
            if(selection.length() > 0)
                selection += " AND ";
            selection += DiskDbContract.ClassGroups.TABLE_NAME + "." + DiskDbContract.ClassGroups.GROUP_ID + "=" + Long.toString(groupId);
        }

        if(teacherId != INVALID_ID) {
            if(selection.length() > 0)
                selection += " AND ";
            selection += DiskDbContract.Class.TABLE_NAME + "." + DiskDbContract.Class.TEACHER_ID + "=" + Long.toString(teacherId);
        }

        if(roomId != INVALID_ID) {
            if(selection.length() > 0)
                selection += " AND ";
            selection += DiskDbContract.ClassRooms.TABLE_NAME + "." + DiskDbContract.ClassRooms.ROOM_ID + "=" + Long.toString(roomId);
        }

        if(selection.length() > 0)
            selection = "WHERE " + selection;

        return "SELECT " +
                Class.TABLE_NAME + "." + Class.TIMESTAMP + " AS " + Schedule.TIMESTAMP + "," +
                ClassName.TABLE_NAME + "." + ClassName.NAME + " AS " + Schedule.NAME + "," +
                Teacher.TABLE_NAME + "." + Teacher.NAME + " AS " + Schedule.TEACHER + "," +
                Comment.TABLE_NAME + "." + Comment.TEXT + " AS " + Schedule.COMMENT + "," +
                Status.TABLE_NAME + "." + Status.STATUS + " AS " + Schedule.STATUS + "," +
                "GROUP_CONCAT(DISTINCT " + Group.TABLE_NAME + "." + Group.NAME + ") AS " + Schedule.GROUPS + "," +
                "GROUP_CONCAT(DISTINCT " + Room.TABLE_NAME + "." + Room.NAME + ") AS " + Schedule.ROOMS + " " +
                "FROM " + Class.TABLE_NAME + " " +
                "INNER JOIN " + ClassName.TABLE_NAME + " ON " + ClassName.TABLE_NAME + "." + ClassName._ID + "=" + Class.TABLE_NAME + "." + Class.NAME_ID + " " +
                "INNER JOIN " + Teacher.TABLE_NAME + " ON " + Teacher.TABLE_NAME + "." + Teacher._ID + "=" + Class.TABLE_NAME + "." + Class.TEACHER_ID + " " +
                "LEFT JOIN " + Comment.TABLE_NAME + " ON " + Comment.TABLE_NAME + "." + Comment._ID + "=" + Class.TABLE_NAME + "." + Class.COMMENT_ID + " " +
                "LEFT JOIN " + Status.TABLE_NAME + " ON " + Status.TABLE_NAME + "." + Status._ID + "=" + Class.TABLE_NAME + "." + Class.STATUS_ID + " " +
                "INNER JOIN " + ClassGroups.TABLE_NAME + " ON " + ClassGroups.TABLE_NAME + "." + ClassGroups.CLASS_ID + "=" + Class.TABLE_NAME + "." + Class._ID + " " +
                "INNER JOIN " + ClassRooms.TABLE_NAME + " ON " + ClassRooms.TABLE_NAME + "." + ClassRooms.CLASS_ID + "=" + Class.TABLE_NAME + "." + Class._ID + " " +
                "INNER JOIN " + Group.TABLE_NAME + " ON " + Group.TABLE_NAME + "." + Group._ID + "=" + ClassGroups.TABLE_NAME + "." + ClassGroups.GROUP_ID + " " +
                "INNER JOIN " + Room.TABLE_NAME + " ON " + Room.TABLE_NAME + "." + Room._ID + "=" + ClassRooms.TABLE_NAME + "." + ClassRooms.ROOM_ID + " " +
                selection + " " +
                "GROUP BY " + Class.TABLE_NAME + "." + Class.TIMESTAMP + "," + Class.TABLE_NAME + "." + Class.NAME_ID + "," + Class.TABLE_NAME + "." + Class.TEACHER_ID + ";";
    }
}
