package com.riscript.ttischedule.database;

import android.util.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;

import static com.riscript.ttischedule.database.Database.INVALID_ID;

class ServerDatabase {
    public static final long MAX_QUERYABLE_DAYS = 182;

    private static final int CONNECTION_TIMEOUT = 4000;

//    private static final String JSON_KEY_ROOT = "d";

    private static final String JSON_KEY_EVENTS = "events";
    private static final String JSON_KEY_EVENTS_KEYS = "keys";
    private static final String JSON_KEY_EVENTS_VALUES = "values";
    private static final String JSON_KEY_TEACHERS = "teachers";
    private static final String JSON_KEY_GROUPS = "groups";
    private static final String JSON_KEY_ROOMS = "rooms";

    private static final String JSON_EVENT_KEY_TIME = "time";
    private static final String JSON_EVENT_KEY_ROOM = "room";
    private static final String JSON_EVENT_KEY_GROUPS = "groups";
    private static final String JSON_EVENT_KEY_TEACHER = "teacher";
    private static final String JSON_EVENT_KEY_NAME = "name";
    private static final String JSON_EVENT_KEY_COMMENT = "comment";
    private static final String JSON_EVENT_KEY_STATUS = "class";

    private static final int JSON_EVENT_KEY_ID_TIME = 0;
    private static final int JSON_EVENT_KEY_ID_ROOMS = 1;
    private static final int JSON_EVENT_KEY_ID_GROUPS = 2;
    private static final int JSON_EVENT_KEY_ID_TEACHER = 3;
    private static final int JSON_EVENT_KEY_ID_NAME = 4;
    private static final int JSON_EVENT_KEY_ID_COMMENT = 5;
    private static final int JSON_EVENT_KEY_ID_STATUS = 6;
    private static final int JSON_EVENT_KEYS = 7;

    private static final String STATUS_NORMAL = "normal";

    interface OnResponseListener {
        void responseBegin();
        void responseEvent(long time, long[] rooms, long[] groups, long teacher, String name, String comment, String status);
        void responseTeachers(Hashtable<Long, String> teachers);
        void responseGroups(Hashtable<Long, String> groups);
        void responseRooms(Hashtable<Long, String> rooms);
        void responseEnd();
    }

    private final OnResponseListener onResponseListener;

    ServerDatabase(OnResponseListener onResponseListener) {
        this.onResponseListener = onResponseListener;
    }

    OnResponseListener getOnUpdateListener() {
        return onResponseListener;
    }

    void requestEvents(Calendar from, Calendar to, long groupId, long teacherId, long roomId, String language) throws IOException, JSONException {
        if(groupId == INVALID_ID && teacherId == INVALID_ID && roomId == INVALID_ID)
            throw new IllegalArgumentException("At least some of filter parameters must be specified!", null);
        request(QueryBuilder.getEvents(
                from,
                to,
                groupId,
                teacherId,
                roomId,
                language
        ));
    }

    void requestSymbols() throws IOException, JSONException {
        request(QueryBuilder.getItems());
    }

    private void request(String urlString) throws IOException, JSONException {
        HttpURLConnection connection;
        final URL url = new URL(urlString);
        connection = (HttpURLConnection)url.openConnection();
        connection.setConnectTimeout(CONNECTION_TIMEOUT);

        onResponseListener.responseBegin();
        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            readJsonStream(connection.getInputStream());
        }
        onResponseListener.responseEnd();
    }

    private void readJsonStream(InputStream jsonStream) throws IOException, JSONException {
        BufferedReader r = new BufferedReader(new InputStreamReader(jsonStream, "UTF-8"));
        StringBuilder sb = new StringBuilder(jsonStream.available());
        String line;
        while((line = r.readLine()) != null) {
            sb.append(line);
            // '\n' is not required for JSON
        }

        String jsonString = sb.toString();

        // Stupid service devs...
        // copy-pasta from the javascript code of old app
        jsonString = jsonString.replaceAll("(\\)\\()", "");
        jsonString = jsonString.replaceAll("\\\\\"", "\"");
        jsonString = jsonString.replaceAll("\\(\\{\\\"d\\\":\\\"", "");
        jsonString = jsonString.replaceAll("\\\"\\}\\)", "");

        readJson(new JSONObject(jsonString));
    }

    private void readJson(JSONObject object) throws JSONException {
        if(object.has(JSON_KEY_EVENTS)) {
            readEvents(object.getJSONObject(JSON_KEY_EVENTS));
        }
        Hashtable<Long, String> dict;
        if(object.has(JSON_KEY_TEACHERS)) {
            dict = readJsonKeyValues(object.getJSONObject(JSON_KEY_TEACHERS));
            onResponseListener.responseTeachers(dict);
        }
        if(object.has(JSON_KEY_GROUPS)) {
            dict = readJsonKeyValues(object.getJSONObject(JSON_KEY_GROUPS));
            onResponseListener.responseGroups(dict);
        }
        if(object.has(JSON_KEY_ROOMS)) {
            dict = readJsonKeyValues(object.getJSONObject(JSON_KEY_ROOMS));
            onResponseListener.responseRooms(dict);
        }
    }

    private void readEvents(JSONObject jsonObject) throws JSONException {
        JSONArray keys = jsonObject.getJSONArray(JSON_KEY_EVENTS_KEYS);
        JSONArray values = jsonObject.getJSONArray(JSON_KEY_EVENTS_VALUES);

        if(keys.length() != JSON_EVENT_KEYS)
            return;

        int[] keyOrder = new int[JSON_EVENT_KEYS];

        for(int i = 0; i < keys.length(); i++) {
            String key = keys.getString(i);
            switch(key) {
                case JSON_EVENT_KEY_TIME: keyOrder[JSON_EVENT_KEY_ID_TIME] = i; break;
                case JSON_EVENT_KEY_ROOM: keyOrder[JSON_EVENT_KEY_ID_ROOMS] = i; break;
                case JSON_EVENT_KEY_GROUPS: keyOrder[JSON_EVENT_KEY_ID_GROUPS] = i; break;
                case JSON_EVENT_KEY_TEACHER: keyOrder[JSON_EVENT_KEY_ID_TEACHER] = i; break;
                case JSON_EVENT_KEY_NAME: keyOrder[JSON_EVENT_KEY_ID_NAME] = i; break;
                case JSON_EVENT_KEY_COMMENT: keyOrder[JSON_EVENT_KEY_ID_COMMENT] = i; break;
                case JSON_EVENT_KEY_STATUS: keyOrder[JSON_EVENT_KEY_ID_STATUS] = i; break;
                default: keyOrder[i] = i;
            }
        }

        for(int i = 0; i < values.length(); i++) {
            JSONArray event = values.getJSONArray(i);
            readEvent(event, keyOrder);
        }
    }

    private void readEvent(JSONArray event, int[] keyOrder) throws JSONException {
        long time = event.getLong(keyOrder[JSON_EVENT_KEY_ID_TIME]);
        JSONArray jsonRooms = event.getJSONArray(keyOrder[JSON_EVENT_KEY_ID_ROOMS]);
        JSONArray jsonGroups = event.getJSONArray(keyOrder[JSON_EVENT_KEY_ID_GROUPS]);
        long teacher;
        try {
            teacher = event.getLong(keyOrder[JSON_EVENT_KEY_ID_TEACHER]);
        } catch(JSONException e) {
            // TODO: check if type mismatch, else throw it again
            teacher = INVALID_ID;
        }
        String name = event.getString(keyOrder[JSON_EVENT_KEY_ID_NAME]);
        String comment = event.getString(keyOrder[JSON_EVENT_KEY_ID_COMMENT]);
        String status = event.getString(keyOrder[JSON_EVENT_KEY_ID_STATUS]);

        if(status.equals(STATUS_NORMAL)) {
            status = "";
        }

        long[] rooms = jsonArrayToLong(jsonRooms);
        long[] groups = jsonArrayToLong(jsonGroups);

        if(rooms == null || rooms.length == 0)
            rooms = new long[] { INVALID_ID };

        if(groups == null || groups.length == 0)
            groups = new long[] { INVALID_ID };

        onResponseListener.responseEvent(time * 1000, rooms, groups, teacher, name, comment, status);
    }

    private long[] jsonArrayToLong(JSONArray jsonArray) throws JSONException {
        long[] longs = new long[jsonArray.length()];
        for(int i = 0; i < jsonArray.length(); i++)
            longs[i] = jsonArray.getLong(i);
        return longs;
    }

    private Hashtable<Long, String> readJsonKeyValues(JSONObject jsonObject) throws JSONException {
        Hashtable<Long, String> table = new Hashtable<>();
        Iterator<String> iter = jsonObject.keys();
        while(iter.hasNext()) {
            String strKey = iter.next();
            long key = Long.parseLong(strKey);
            String value = jsonObject.getString(strKey);
            table.put(key, value);
        }
        return table;
    }

    private static class QueryBuilder {
        private static final String URL_ITEMS =
                "http://services.tsi.lv/schedule/api/service.asmx/GetItems";

        private static final String URL_EVENTS =
                "http://services.tsi.lv/schedule/api/service.asmx/GetLocalizedEvents";

        private static final String URL_EVENT_PARAM_FROM = "from";
        private static final String URL_EVENT_PARAM_TO = "to";
        private static final String URL_EVENT_PARAM_GROUP = "groups";
        private static final String URL_EVENT_PARAM_TEACHER = "teachers";
        private static final String URL_EVENT_PARAM_ROOM = "rooms";
        private static final String URL_EVENT_PARAM_LANG = "lang";

        static String getItems() {
            return URL_ITEMS;
        }

        static String getEvents(Calendar from, Calendar to, long group, long teacher, long room, String language) {
            long ts_from = from.getTimeInMillis() / 1000;
            long ts_to = to.getTimeInMillis() / 1000;
            String szGroup = group != INVALID_ID ? Long.toString(group) : "";
            String szTeacher = teacher != INVALID_ID ? Long.toString(teacher) : "";
            String szRoom = room != INVALID_ID ? Long.toString(room) : "";
            return URL_EVENTS +
                    "?" + URL_EVENT_PARAM_FROM    + "=" + ts_from +
                    "&" + URL_EVENT_PARAM_TO      + "=" + ts_to +
                    "&" + URL_EVENT_PARAM_GROUP   + "=" + szGroup +
                    "&" + URL_EVENT_PARAM_TEACHER + "=" + szTeacher +
                    "&" + URL_EVENT_PARAM_ROOM    + "=" + szRoom +
                    "&" + URL_EVENT_PARAM_LANG    + "=" + "\"" + language + "\"";
        }
    }
}
