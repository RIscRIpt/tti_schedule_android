package com.riscript.ttischedule.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.riscript.ttischedule.database.Database.INVALID_ID;

public class DiskDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "schedule.db";

    public static final int MIN_GROUP_COUNT = 1;
    public static final int MIN_ROOM_COUNT = 1;
    public static final int MIN_TEACHER_COUNT = 1;

    DiskDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        db.execSQL(DiskDbContract.SQL_CREATE_GROUP);
        db.execSQL(DiskDbContract.SQL_CREATE_ROOM);
        db.execSQL(DiskDbContract.SQL_CREATE_TEACHER);
        db.execSQL(DiskDbContract.SQL_CREATE_CLASSNAME);
        db.execSQL(DiskDbContract.SQL_CREATE_COMMENT);
        db.execSQL(DiskDbContract.SQL_CREATE_STATUS);
        db.execSQL(DiskDbContract.SQL_CREATE_CLASS);
        db.execSQL(DiskDbContract.SQL_CREATE_CLASSGROUPS);
        db.execSQL(DiskDbContract.SQL_CREATE_CLASSROOMS);
        db.execSQL(DiskDbContract.SQL_CREATE_FILTER);
        db.setTransactionSuccessful();
        db.endTransaction();

        createNullEntries(db);
    }

    public void createNullEntries(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put(DiskDbContract.ClassName._ID, 0);
        cv.put(DiskDbContract.ClassName.NAME, "");
        insert(db, DiskDbContract.ClassName.TABLE_NAME, cv);

        cv.clear();
        cv.put(DiskDbContract.Comment._ID, 0);
        cv.put(DiskDbContract.Comment.TEXT, "");
        insert(db, DiskDbContract.Comment.TABLE_NAME, cv);

        cv.clear();
        cv.put(DiskDbContract.Status._ID, 0);
        cv.put(DiskDbContract.Status.STATUS, "");
        insert(db, DiskDbContract.Status.TABLE_NAME, cv);

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Disk database is only a cache for server database, so its update policy is
        // to simply discard the data and start over
        db.beginTransaction();
        db.execSQL(DiskDbContract.SQL_DELETE_GROUP);
        db.execSQL(DiskDbContract.SQL_DELETE_ROOM);
        db.execSQL(DiskDbContract.SQL_DELETE_TEACHER);
        db.execSQL(DiskDbContract.SQL_DELETE_CLASSNAME);
        db.execSQL(DiskDbContract.SQL_DELETE_COMMENT);
        db.execSQL(DiskDbContract.SQL_DELETE_STATUS);
        db.execSQL(DiskDbContract.SQL_DELETE_CLASS);
        db.execSQL(DiskDbContract.SQL_DELETE_CLASSGROUPS);
        db.execSQL(DiskDbContract.SQL_DELETE_CLASSROOMS);
        db.execSQL(DiskDbContract.SQL_DELETE_FILTER);
        db.setTransactionSuccessful();
        db.endTransaction();
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private long insert(SQLiteDatabase db, String table, ContentValues values) {
        // This database is only a cache for online data,
        // so on conflict replace with the new data
        return db.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

}
