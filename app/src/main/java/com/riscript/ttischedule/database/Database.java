package com.riscript.ttischedule.database;

import android.content.Context;

import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.TimeZone;

import static com.riscript.ttischedule.database.DiskDbHelper.MIN_GROUP_COUNT;
import static com.riscript.ttischedule.database.DiskDbHelper.MIN_ROOM_COUNT;
import static com.riscript.ttischedule.database.DiskDbHelper.MIN_TEACHER_COUNT;

public class Database
    implements ServerDatabase.OnResponseListener
{
    public static final long INVALID_ID = -1;

    private ServerDatabase serverDatabase;
    private MemoryDatabase memoryDatabase;
    private DiskDatabase diskDatabase;

    private Filters filters;

    public Database(Context context) {
        this.serverDatabase = new ServerDatabase(this);
        this.memoryDatabase = new MemoryDatabase();
        this.diskDatabase = new DiskDatabase(context);
    }

    public void clear() {
        if(filters != null) {
            filters.clear();
        }
        memoryDatabase.clear();
        diskDatabase.clear();
    }

    public Filters getFilters() {
        if(filters == null)
            filters = new Filters(this, diskDatabase.getFilters());
        return filters;
    }

    public Filter getFilterById(long filterId) {
        Filter filter = getFilters().getById(filterId);
        if(filter == null)
            filter = makeFilter();
        return filter;
    }

    public Symbols getSymbols(boolean refetch) throws Exception {
        if(!memoryDatabase.hasSymbols() || refetch) {
            if(!diskDatabase.hasSymbols() || refetch) {
                serverDatabase.requestSymbols();
            } else {
                memoryDatabase.setGroups(diskDatabase.getGroups());
                memoryDatabase.setTeachers(diskDatabase.getTeachers());
                memoryDatabase.setRooms(diskDatabase.getRooms());
            }
        }
        return new Symbols(
                memoryDatabase.getGroups(),
                memoryDatabase.getTeachers(),
                memoryDatabase.getRooms()
        );
    }

    public Schedule getSchedule(long filterId) throws Exception {
        Filter filter = getFilters().getById(filterId);
        if(filter == null)
            throw new InvalidKeyException("Filter with such id doesn't exist!");
        if(!filter.hasSchedule()) {
            DiskDatabase.Filter diskFilter = filter.diskDbFilter;
            Calendar from = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            Calendar to = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            setTimeFromTo(from, to);
            long gid = diskFilter.group.getId();
            long tid = diskFilter.teacher.getId();
            long rid = diskFilter.room.getId();
            String lang = "en";
            serverDatabase.requestEvents(from, to, gid, tid, rid, lang);
            filter.setHasSchedule(true);
        }
        return new Schedule(
                filter,
                diskDatabase.getSchedule(filter.diskDbFilter)
        );
    }

    private void setTimeFromTo(Calendar from, Calendar to) {
        int month = from.get(Calendar.MONTH);
        if(month <= Calendar.JANUARY || month >= Calendar.AUGUST) {
            // First semester
            from.set(Calendar.MONTH, Calendar.SEPTEMBER);
        } else {
            // Second semester
            from.set(Calendar.MONTH, Calendar.FEBRUARY);
        }
        from.set(Calendar.DAY_OF_MONTH, 1);
        from.set(Calendar.HOUR_OF_DAY, 0);
        from.set(Calendar.MINUTE, 0);
        from.set(Calendar.SECOND, 0);
        from.set(Calendar.MILLISECOND, 0);

        to.setTimeInMillis(from.getTimeInMillis()
                + ServerDatabase.MAX_QUERYABLE_DAYS * 24L * 60L * 60L * 1000L
        );
    }

    public static class Class {
        public final Calendar time;
        public final String name;
        public final String teacher;
        public final String comment;
        public final String status;
        public final String[] groups;
        public final String[] rooms;

        public Class(Calendar time, String name, String teacher, String comment, String status, String[] groups, String[] rooms) {
            this.time = time;
            this.name = name;
            this.teacher = teacher;
            this.comment = comment;
            this.status = status;
            this.groups = groups;
            this.rooms = rooms;
        }
    }

    public Filter makeFilter() {
        return new Filter(diskDatabase.makeSyncedFilter());
    }
    public Filter makeFilter(DiskDatabase.Filter filter) {
        return new Filter(filter);
    }
    public Filter makeFilter(String group, String teacher, String room) {
        return new Filter(diskDatabase.makeSyncedFilter(group, teacher, room));
    }

    public class Filter {
        final DiskDatabase.Filter diskDbFilter;

        Filter(DiskDatabase.Filter diskDbFilter) {
            this.diskDbFilter = diskDbFilter;
        }

        public long getId() {
            return diskDbFilter.id;
        }

        public String getGroup() {
            return diskDbFilter.group.getValue();
        }
        public String getTeacher() {
            return diskDbFilter.teacher.getValue();
        }
        public String getRoom() {
            return diskDbFilter.room.getValue();
        }

        public DiskDatabase.RefValue getGroupRefValue() {
            return diskDbFilter.group;
        }
        public DiskDatabase.RefValue getTeacherRefValue() {
            return diskDbFilter.teacher;
        }
        public DiskDatabase.RefValue getRoomRefValue() {
            return diskDbFilter.room;
        }

        public boolean isEmpty() {
            return diskDbFilter.group.isEmpty()
                    && diskDbFilter.teacher.isEmpty()
                    && diskDbFilter.room.isEmpty();
        }

        public boolean save() {
            boolean saved = diskDbFilter.save();
            if(saved) {
                if(filters != null) {
                    filters.add(diskDbFilter);
                }
            }
            return saved;
        }

        public boolean isValid() {
            return !isEmpty()
                    && (diskDbFilter.group.isEmpty() || diskDbFilter.group.compile())
                    && (diskDbFilter.teacher.isEmpty() || diskDbFilter.teacher.compile())
                    && (diskDbFilter.room.isEmpty() || diskDbFilter.room.compile());
        }

        boolean hasSchedule() {
            return diskDbFilter.has_schedule;
        }

        public void setHasSchedule(boolean has) {
            diskDbFilter.setHasSchedule(has);
        }

        public String getTitle() {
            StringBuilder sb = new StringBuilder();
            if(getRoom() != null) {
                if(sb.length() > 0) sb.append(", ");
                sb.append(getRoom());
            }
            if(getTeacher() != null) {
                if(sb.length() > 0) sb.append(", ");
                sb.append(getTeacher());
            }
            if(getGroup() != null) {
                if(sb.length() > 0) sb.append(", ");
                sb.append(getGroup());
            }
            return sb.toString();
        }
    }

    private long getClassNameId(String name) {
        long id = memoryDatabase.getClassNameId(name);
        if(id == INVALID_ID) {
            id = diskDatabase.retrieveClassNameId(name);
            memoryDatabase.setClassNameId(name, id);
        }
        return id;
    }

    private long getCommentId(String comment) {
        long id = memoryDatabase.getCommentId(comment);
        if(id == INVALID_ID) {
            id = diskDatabase.retrieveCommentId(comment);
            memoryDatabase.setCommentId(comment, id);
        }
        return id;
    }

    private long getStatusId(String status) {
        long id = memoryDatabase.getStatusId(status);
        if(id == INVALID_ID) {
            id = diskDatabase.retrieveStatusId(status);
            memoryDatabase.setStatusId(status, id);
        }
        return id;
    }

    @Override
    public void responseBegin() {

    }

    @Override
    public void responseEvent(long time, long[] rooms, long[] groups, long teacher, String name, String comment, String status) {
        long classNameId = getClassNameId(name);
        long commentId = getCommentId(comment);
        long statusId = getStatusId(status);

        diskDatabase.updateClass(time, rooms, groups, teacher, classNameId, commentId, statusId);
    }

    @Override
    public void responseTeachers(Hashtable<Long, String> teachers) {
        memoryDatabase.setTeachers(teachers);
        diskDatabase.clearTableTeachers();
        DiskDatabase.TeacherInserter inserter = diskDatabase.getTeacherInserter();
        inserter.begin();
        for(Hashtable.Entry<Long, String> teacher: teachers.entrySet()) {
            inserter.insert(teacher.getKey(), teacher.getValue());
        }
        inserter.end();
    }

    @Override
    public void responseGroups(Hashtable<Long, String> groups) {
        memoryDatabase.setGroups(groups);
        diskDatabase.clearTableGroups();
        DiskDatabase.GroupInserter inserter = diskDatabase.getGroupInserter();
        inserter.begin();
        for(Hashtable.Entry<Long, String> group: groups.entrySet()) {
            inserter.insert(group.getKey(), group.getValue());
        }
        inserter.end();
    }

    @Override
    public void responseRooms(Hashtable<Long, String> rooms) {
        memoryDatabase.setRooms(rooms);
        diskDatabase.clearTableRooms();
        DiskDatabase.RoomInserter inserter = diskDatabase.getRoomInserter();
        inserter.begin();
        for(Hashtable.Entry<Long, String> room: rooms.entrySet()) {
            inserter.insert(room.getKey(), room.getValue());
        }
        inserter.end();
    }

    @Override
    public void responseEnd() {

    }

}
