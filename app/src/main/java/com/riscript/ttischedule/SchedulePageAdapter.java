package com.riscript.ttischedule;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.DiskDbContract;
import com.riscript.ttischedule.database.Filters;
import com.riscript.ttischedule.database.Schedule;

import java.util.ArrayList;
import java.util.Collection;

public class SchedulePageAdapter
        extends FragmentStatePagerAdapter
        implements Filters.DataSetChangeListener
{
    interface PageCountChangeListener {
        void onPageCountChanged(int newCount);
    }

    private Filters filters;
    private ScheduleRecyclerView[] scheduleViews;
    private PageCountChangeListener pageCountChangeListener;

    SchedulePageAdapter(FragmentManager fm) {
        super(fm);
    }

    public PageCountChangeListener getPageCountChangeListener() {
        return pageCountChangeListener;
    }

    public void setPageCountChangeListener(PageCountChangeListener listener) {
        this.pageCountChangeListener = listener;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
        if(filters != null)
            filters.subscribeDataSetChange(this);
        onDataSetChange();
    }

    @Override
    public Fragment getItem(int position) {
        if(getCount() == 0)
            return null;

        ScheduleRecyclerView view = scheduleViews[position];
        if(view == null) {
            Bundle args = new Bundle();
            args.putLong(ScheduleRecyclerView.ARG_DATA_SOURCE, getFilter(position).getId());
            view = new ScheduleRecyclerView();
            view.setArguments(args);
            view.setRetainInstance(true);
            scheduleViews[position] = view;
        }
        return view;
    }

    public Database.Filter getFilter(int position) {
        return filters.getByOrder(position);
    }

    @Override
    public int getCount() {
        if(filters == null)
            return 0;
        return filters.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    // http://stackoverflow.com/a/14849050/1901561
    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void onDataSetChange() {
        scheduleViews = null;
        if(filters != null)
            scheduleViews = new ScheduleRecyclerView[filters.size()];

        notifyDataSetChanged();

        if(pageCountChangeListener != null)
            pageCountChangeListener.onPageCountChanged(getCount());
    }

    @Override
    public void onMove(int fromPosition, int toPosition) {
        //TODO: just move
        onDataSetChange();
    }

    public void destroy() {
        if(filters != null)
            filters.unsubscribeDataSetChange(this);
    }

    public void refreshFilter(int position) {
        //TODO: make this more effitient
        final Database.Filter filter = getFilter(position);
        filter.setHasSchedule(false);
        ScheduleRecyclerView scheduleView = scheduleViews[position];
        if(scheduleView != null) {
            scheduleView.reloadSchedule();
        }
    }
}
