package com.riscript.ttischedule;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;

import java.util.Calendar;
import java.util.TimeZone;

public abstract class DateRecyclerViewAdapter<T extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<T>
{
    private final Calendar baseDay;
    private final long baseTimestamp;

    protected final TypedValue colorEven;
    protected final TypedValue colorOdd;

    DateRecyclerViewAdapter(Context ctx, long baseDayMillis) {
        baseDay = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        if(baseDayMillis != -1)
            baseDay.setTimeInMillis(baseDayMillis);
        baseDay.set(Calendar.HOUR_OF_DAY, 0);
        baseDay.set(Calendar.MINUTE, 0);
        baseDay.set(Calendar.SECOND, 0);
        baseDay.set(Calendar.MILLISECOND, 0);

        baseTimestamp = baseDay.getTimeInMillis();

        colorEven = new TypedValue();
        colorOdd = new TypedValue();

        Resources.Theme theme = ctx.getTheme();
        theme.resolveAttribute(R.attr.colorEven, colorEven, true);
        theme.resolveAttribute(R.attr.colorOdd, colorEven, true);
    }

    // returns modified baseDay
    // not thread safe!
    private Calendar positionToDate(int position) {
        baseDay.setTimeInMillis(baseTimestamp);
        baseDay.add(Calendar.DAY_OF_YEAR, position - DateLayoutManager.BASE_POSITION);
        return baseDay;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        onBindViewHolder(holder, positionToDate(position));
    }

    public abstract void onBindViewHolder(T holder, Calendar day);
}
