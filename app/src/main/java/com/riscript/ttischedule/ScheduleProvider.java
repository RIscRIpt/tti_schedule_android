package com.riscript.ttischedule;

import android.content.Context;
import android.os.AsyncTask;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.Filters;
import com.riscript.ttischedule.database.Schedule;
import com.riscript.ttischedule.database.Symbols;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

final class ScheduleProvider {
    interface InitializedListener {
        void onInitialized();
    }
    interface ErrorListener {
        void onError(String message);
    }
    interface ResponseSymbolsListener {
        void onSymbolsRetrieved(Symbols symbols);
    }
    interface ResponseFiltersListener {
        void onFiltersRetrieved(Filters filters);
    }
    interface ResponseFilterListener {
        void onFilterReceived(Database.Filter filter);
    }
    interface ResponseScheduleListener {
        void onScheduleReceived(Schedule schedule);
    }
    interface ClearedListener {
        void onCleared();
    }

    private enum Command {
        INITIALIZE,

        REQUEST_FILTERS,
        REQUEST_SYMBOLS,
        REQUEST_SCHEDULE,

        REQUEST_FILTER,

        REQUEST_CLEAR,
    }

    private class CommandWrapper {
        final Object listener;
        final Command command;
        final Object[] params;

        private CommandWrapper(Object listener, Command command, Object[] params) {
            this.listener = listener;
            this.command = command;
            this.params = params;
        }
    }

    private class RequestHandler extends AsyncTask<Object, Void, Object> {
        private final CommandWrapper command;

        private RequestHandler(CommandWrapper command) {
            this.command = command;
        }

        @Override
        protected Object doInBackground(Object... params) {
            switch(command.command) {
                case INITIALIZE: return initialize(params);
                case REQUEST_FILTERS: return retrieveFilters(params);
                case REQUEST_SYMBOLS: return retrieveSymbols(params);
                case REQUEST_SCHEDULE: return retrieveSchedule(params);
                case REQUEST_FILTER: return retrieveFilter(params);
                case REQUEST_CLEAR: return clear(params);
                default: return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            switch(command.command) {
                case INITIALIZE:
                    responseInitialize((InitializedListener)command.listener);
                    break;
                case REQUEST_FILTERS:
                    responseFilters((ResponseFiltersListener)command.listener, (Filters)o);
                    break;
                case REQUEST_SYMBOLS:
                    if(o instanceof Symbols) {
                        responseSymbols((ResponseSymbolsListener)command.listener, (Symbols)o);
                    } else if(o instanceof Exception) {
                        responseError((Exception)o);
                    }
                    break;
                case REQUEST_SCHEDULE:
                    if(o instanceof Schedule) {
                        responseSchedule((ResponseScheduleListener)command.listener, (Schedule)o);
                    } else if(o instanceof Exception) {
                        responseError((Exception)o);
                    }
                    break;
                case REQUEST_FILTER:
                    responseFilter((ResponseFilterListener)command.listener, (Database.Filter)o);
                    break;
                case REQUEST_CLEAR:
                    responseClear((ClearedListener)command.listener);
                    break;
            }

            synchronized(queueMutex) {
                requestHandler = null;
            }
            executeNextCommand();
        }

        private Object initialize(Object[] params) {
            Context context = (Context)params[0];
            if(database == null) {
                database = new Database(context);
            }
            return null;
        }

        private Object retrieveFilters(Object[] params) {
            return database.getFilters();
        }

        private Object retrieveSymbols(Object[] params) {
            try {
                return database.getSymbols((boolean)params[0]);
            } catch(Exception e) {
                return e;
            }
        }

        private Object retrieveSchedule(Object[] params) {
            long filterId = (long)params[0];
            try {
                return database.getSchedule(filterId);
            } catch(Exception e) {
                return e;
            }
        }

        private Object retrieveFilter(Object[] params) {
            long filterId = -1;
            if(params.length > 0)
                filterId = (long)params[0];
            if(filterId != -1)
                return database.getFilterById(filterId);
            else
                return database.makeFilter(null, null, null);
        }

        private Object clear(Object[] params) {
            database.clear();
            return null;
        }

    }

    private void responseInitialize(InitializedListener listener) {
        listener.onInitialized();
    }

    private void responseError(Exception e) {
        for(ErrorListener el : errorListeners) {
            el.onError(e.getMessage());
        }
    }

    private void responseFilters(ResponseFiltersListener listener, Filters filters) {
        listener.onFiltersRetrieved(filters);
    }

    private void responseSymbols(ResponseSymbolsListener listener, Symbols symbols) {
        listener.onSymbolsRetrieved(symbols);
    }

    private void responseSchedule(ResponseScheduleListener listener, Schedule schedule) {
        listener.onScheduleReceived(schedule);
    }

    private void responseFilter(ResponseFilterListener listener, Database.Filter filter) {
        listener.onFilterReceived(filter);
    }

    private void responseClear(ClearedListener listener) {
        for(ClearedListener cl : clearedListeners) {
            cl.onCleared();
        }
        listener.onCleared();
    }

    private static volatile ScheduleProvider instance;
    static ScheduleProvider getInstance() {
        if(instance == null) {
            synchronized(ScheduleProvider.class) {
                if(instance == null) {
                    instance = new ScheduleProvider();
                }
            }
        }
        return instance;
    }

    private final Collection<ErrorListener> errorListeners;
    private final Collection<ClearedListener> clearedListeners;

    private final ConcurrentLinkedQueue<CommandWrapper> commandQueue;
    private final Object queueMutex;

    private RequestHandler requestHandler;
    private Database database;

    private ScheduleProvider() {
        errorListeners = new ArrayList<>(2);
        clearedListeners = new ArrayList<>(2);

        commandQueue = new ConcurrentLinkedQueue<>();
        queueMutex = new Object();
    }

    ScheduleProvider subscribeErrorListener(ErrorListener listener) {
        errorListeners.add(listener);
        return this;
    }
    ScheduleProvider unsubscribeErrorListener(ErrorListener listener) {
        errorListeners.remove(listener);
        return this;
    }
    ScheduleProvider subscribeClearedListener(ClearedListener listener) {
        clearedListeners.add(listener);
        return this;
    }
    ScheduleProvider unsubscribeClearedListener(ClearedListener listener) {
        clearedListeners.remove(listener);
        return this;
    }

    ScheduleProvider initialize(InitializedListener listener, Context context) {
        if(database == null) {
            postCommand(listener, Command.INITIALIZE, context);
        }
        return this;
    }
    void requestFilters(ResponseFiltersListener listener) {
        postCommand(listener, Command.REQUEST_FILTERS);
    }
    void requestSymbols(ResponseSymbolsListener listener, boolean refetch) {
        postCommand(listener, Command.REQUEST_SYMBOLS, refetch);
    }
    void requestFilter(ResponseFilterListener listener, long id) {
        postCommand(listener, Command.REQUEST_FILTER, id);
    }
    void requestSchedule(ResponseScheduleListener listener, long filterId) {
        postCommand(listener, Command.REQUEST_SCHEDULE, filterId);
    }
    void requestClear(ClearedListener listener) {
        postCommand(listener, Command.REQUEST_CLEAR);
    }

    private void postCommand(Object listener, Command command, Object... params) {
        commandQueue.add(new CommandWrapper(listener, command, params));
        executeNextCommand();
    }

    private void executeNextCommand() {
        synchronized(queueMutex) {
            if(requestHandler == null){
                CommandWrapper nextCommand = commandQueue.poll();
                if(nextCommand != null) {
                    requestHandler = new RequestHandler(nextCommand);
                    requestHandler.execute(nextCommand.params);
                }
            }
        }
    }

    public void cancelAllRequests(Object listener) {
        synchronized(queueMutex) {
            Iterator<CommandWrapper> iter = commandQueue.iterator();
            while(iter.hasNext()) {
                CommandWrapper wrapper = iter.next();
                if(wrapper.listener == listener)
                    iter.remove();
            }
        }
    }


}
