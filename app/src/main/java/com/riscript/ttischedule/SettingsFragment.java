package com.riscript.ttischedule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SettingsFragment
        extends android.support.v4.app.Fragment
{
    public static final String ARGUMENT_TITLE = "title";
    public static final String ARGUMENT_PARAM = "param";
    public static final String ARGUMENT_HAS_NAVIGATION = "has_nav";

    public static final long PARAM_NONE = -1;


    protected SettingsActivity settingsActivity;

    private String title;

    public String getTitle() {
        Bundle args = getArguments();
        if(args == null)
            return null;
        return args.getString(ARGUMENT_TITLE);
    }

    public long getParam() {
        Bundle args = getArguments();
        if(args == null)
            return PARAM_NONE;
        return args.getLong(ARGUMENT_PARAM);
    }

    public void setTitle(String title) {
        Bundle args = getArguments();
        if(args == null) {
            args = new Bundle();
        }
        args.putString(ARGUMENT_TITLE, title);
        setArguments(args);
    }

    public void setParam(long value) {
        Bundle args = getArguments();
        if(args == null) {
            args = new Bundle();
        }
        args.putLong(ARGUMENT_PARAM, value);
        setArguments(args);
    }

    public boolean hasNavigationBar() {
        Bundle args = getArguments();
        if(args == null)
            return true;
        return args.getBoolean(ARGUMENT_HAS_NAVIGATION);
    }

    public void setHasNavigation(boolean has) {
        Bundle args = getArguments();
        if(args == null) {
            args = new Bundle();
        }
        args.putBoolean(ARGUMENT_HAS_NAVIGATION, has);
        setArguments(args);
    }

    public void initialize() {
        settingsActivity = (SettingsActivity)getActivity();
    }
}
