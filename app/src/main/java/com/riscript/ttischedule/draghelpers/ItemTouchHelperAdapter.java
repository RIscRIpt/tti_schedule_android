package com.riscript.ttischedule.draghelpers;

public interface ItemTouchHelperAdapter {
    boolean canMoveItem(int position);
    boolean canDismissItem(int position);

    boolean onItemMove(int fromPosition, int toPosition);
    void onItemDismiss(int position);
}
