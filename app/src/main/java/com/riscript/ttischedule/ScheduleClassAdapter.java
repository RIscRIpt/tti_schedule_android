package com.riscript.ttischedule;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.riscript.ttischedule.database.Database;

import java.util.Calendar;
import java.util.Locale;

//public class ScheduleClassAdapter extends ArrayAdapter<Database.Class> {
//    private static class ViewHolder {
//        final TextView
//                time,
//                rooms,
//                name,
//                status,
//                comment,
//                teacher,
//                groups;
//
//        ViewHolder(View lesson) {
//            time = (TextView)lesson.findViewById(R.id.sc_time);
//            rooms = (TextView)lesson.findViewById(R.id.sc_rooms);
//            name = (TextView)lesson.findViewById(R.id.sc_name);
//            status = (TextView)lesson.findViewById(R.id.sc_status);
//            comment = (TextView)lesson.findViewById(R.id.sc_comment);
//            teacher = (TextView)lesson.findViewById(R.id.sc_teacher);
//            groups = (TextView)lesson.findViewById(R.id.sc_groups);
//        }
//
//        void bindTo(Database.Class c) {
//            int hour = c.time.get(Calendar.HOUR_OF_DAY);
//            int minutes = c.time.get(Calendar.MINUTE);
//
//            time.setText(Integer.toString(hour) + ":" + Integer.toString(minutes));
//            rooms.setText(TextUtils.join(", ", c.rooms));
//            name.setText(c.name);
//            status.setText(c.status);
//            comment.setText(c.comment);
//            teacher.setText(c.teacher);
//            groups.setText(TextUtils.join(", ", c.groups));
//
//            rooms.setVisibility(c.rooms.length == 0 ? View.GONE : View.VISIBLE);
//            name.setVisibility(c.name.length()==0?View.GONE:View.VISIBLE);
//            status.setVisibility(c.status.length()==0?View.GONE:View.VISIBLE);
//            comment.setVisibility(c.comment.length()==0?View.GONE:View.VISIBLE);
//            teacher.setVisibility(c.teacher.length()==0?View.GONE:View.VISIBLE);
//            groups.setVisibility(c.groups.length==0?View.GONE:View.VISIBLE);
//        }
//    }
//
//    private Database.Class[] classes;
//
//    public ScheduleClassAdapter(@NonNull Context context) {
//        this(context, null);
//    }
//
//    public ScheduleClassAdapter(@NonNull Context context, Database.Class[] classesOfDay) {
//        super(context, R.layout.schedule_class);
//        setClasses(classesOfDay);
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder viewHolder;
//        if(convertView == null) {
//            convertView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.schedule_class, parent, false);
//            viewHolder = new ViewHolder(convertView);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder)convertView.getTag();
//        }
//
//        viewHolder.bindTo(classes[position]);
//
//        return convertView;
//    }
//
//    @Override
//    public int getCount() {
//        return classes == null ? 0 : classes.length;
//    }
//
//    public void setClasses(Database.Class[] classes) {
//        this.classes = classes;
//        notifyDataSetChanged();
//    }
//
////    public boolean areAllItemsEnabled() {
////        return false;
////    }
////
////    @Override
////    public boolean isEnabled(int position) {
////        return false;
////    }
////
////    @Override
////    public void registerDataSetObserver(DataSetObserver observer) {
////
////    }
////
////    @Override
////    public void unregisterDataSetObserver(DataSetObserver observer) {
////
////    }
////
////    @Override
////    public Object getItem(int position) {
////        return classes[position];
////    }
////
////    @Override
////    public long getItemId(int position) {
////        return position;
////    }
////
////    @Override
////    public boolean hasStableIds() {
////        return false;
////    }
////
//    @Override
//    public int getItemViewType(int position) {
//        return 0;
//    }
//    @Override
//    public int getViewTypeCount() {
//        return 1;
//    }
//    @Override
//    public boolean isEmpty() {
//        return false;
//    }

public class ScheduleClassAdapter extends BaseAdapter {
    private final Context mContext;

    private Database.Class[] mClasses;

    public ScheduleClassAdapter(@NonNull Context context) {
        this(context, null);
    }

    public ScheduleClassAdapter(@NonNull Context context, Database.Class[] classes) {
        mContext = context;
        mClasses = classes;
    }

    public void setClasses(Database.Class[] classes) {
        mClasses = classes;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mClasses != null ? mClasses.length : 0;
    }

    @Override
    public @Nullable Database.Class getItem(int position) {
        return mClasses[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.schedule_class, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.bindTo(mClasses[position]);

        return convertView;
    }

    private static class ViewHolder {
        final TextView
                time,
                rooms,
                name,
                status,
                comment,
                teacher,
                groups;

        ViewHolder(View lesson) {
            time = (TextView)lesson.findViewById(R.id.sc_time);
            rooms = (TextView)lesson.findViewById(R.id.sc_rooms);
            name = (TextView)lesson.findViewById(R.id.sc_name);
            status = (TextView)lesson.findViewById(R.id.sc_status);
            comment = (TextView)lesson.findViewById(R.id.sc_comment);
            teacher = (TextView)lesson.findViewById(R.id.sc_teacher);
            groups = (TextView)lesson.findViewById(R.id.sc_groups);
        }

        void bindTo(Database.Class c) {
            int hour = c.time.get(Calendar.HOUR_OF_DAY);
            int minutes = c.time.get(Calendar.MINUTE);

            setTextAndVisibility(time, String.format(Locale.US, "%02d:%02d", hour, minutes));
            setTextAndVisibility(rooms, c.rooms);
            setTextAndVisibility(name, c.name);
            setTextAndVisibility(status, c.status);
            setTextAndVisibility(comment, c.comment);
            setTextAndVisibility(teacher, c.teacher);
            setTextAndVisibility(groups, c.groups);
        }

        static void setTextAndVisibility(TextView tv, String text) {
            tv.setText(text);
            if(text == null || text.length() == 0)
                tv.setVisibility(View.GONE);
            else
                tv.setVisibility(View.VISIBLE);
        }

        static void setTextAndVisibility(TextView tv, String[] textArray) {
            if(textArray != null && textArray.length > 0)
                setTextAndVisibility(tv, TextUtils.join(", ", textArray));
            else
                setTextAndVisibility(tv, (String)null);
        }
    }
}
