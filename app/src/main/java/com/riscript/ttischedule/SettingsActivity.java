package com.riscript.ttischedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import static com.riscript.ttischedule.SettingsFragment.ARGUMENT_PARAM;
import static com.riscript.ttischedule.SettingsFragment.PARAM_NONE;
import static com.riscript.ttischedule.database.Database.INVALID_ID;

public class SettingsActivity
        extends ThemedAppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {
    private static String DEFAULT_TITLE = null;

    public static final String PAGE = "page";

    public static final int PAGE_FILTERS = 0;
    public static final int PAGE_NEW_FILTER = 1;
    public static final int PAGE_EDIT_FILTER = 2;
    public static final int PAGE_CACHE = 3;

    private int currentPage;

    private Toolbar toolbar;
    private BottomNavigationView navigation;
    private SettingsFiltersFragment filtersFragment;
    private SettingsCacheFragment cacheFragment;
    private SettingsEditFilterFragment editFilterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DEFAULT_TITLE = getString(R.string.title_settings);

        setContentView(R.layout.activity_settings);

        toolbar = (Toolbar)findViewById(R.id.settings_toolbar);
        // http://stackoverflow.com/a/35430590/1901561
        toolbar.setTitle(DEFAULT_TITLE);
        setSupportActionBar(toolbar);

        navigation = (BottomNavigationView)findViewById(R.id.settings_navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        Intent intent = getIntent();
        int page = intent.getIntExtra(PAGE, PAGE_FILTERS);
        long param = intent.getLongExtra(ARGUMENT_PARAM, PARAM_NONE);
        navigateTo(page, param, false);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean selected = true;

        switch(item.getItemId()) {
            case R.id.settings_nav_filters:
                navigateTo(PAGE_FILTERS);
                break;
            case R.id.settings_nav_cache:
                navigateTo(PAGE_CACHE);
                break;
            default:
                selected = false;
        }

        return selected;
    }

    @Override
    public void onBackStackChanged() {
        final FragmentManager fm = getSupportFragmentManager();
        SettingsFragment fragment = (SettingsFragment)fm.findFragmentById(R.id.settings_content);
        navigation.setVisibility(fragment.hasNavigationBar() ? View.VISIBLE : View.GONE);
    }

    public void navigateTo(int page) {
        navigateTo(page, PARAM_NONE, false);
    }

    public void navigateTo(int page, long param) {
        navigateTo(page, param, true);
    }

    private SettingsFragment getFragmentByPage(int page, long param) {
        switch(page) {
            default:
            case PAGE_FILTERS: return getFiltersFragment();
            case PAGE_NEW_FILTER: return getEditFilterFragment();
            case PAGE_EDIT_FILTER: return getEditFilterFragment(param);
            case PAGE_CACHE: return getCacheFragment();
        }
    }

    private void navigateTo(int page, long param, boolean canAddToBackStack) {
        SettingsFragment fragment = getFragmentByPage(page, param);
        navigateTo(fragment, canAddToBackStack);
        currentPage = page;
        navigation.setVisibility(fragment.hasNavigationBar() ? View.VISIBLE : View.GONE);
    }


    private void navigateTo(SettingsFragment fragment, boolean addToBackStack) {
        if(fragment.isAdded())
            return;

        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.settings_content, fragment);
        if(addToBackStack)
            transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        String actionBarTitle = DEFAULT_TITLE;
        if(fragment.getTitle() != null)
            actionBarTitle = fragment.getTitle();
        toolbar.setTitle(actionBarTitle);
    }

    public void navigateBack() {
        final FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 0)
            fm.popBackStackImmediate();
        else
            super.onBackPressed();
    }

    private SettingsFragment getFiltersFragment() {
        if(filtersFragment == null) {
            filtersFragment = new SettingsFiltersFragment();
            filtersFragment.setTitle(getString(R.string.title_edit_filters));
            filtersFragment.setHasNavigation(true);
        }
        return filtersFragment;
    }

    private SettingsFragment getEditFilterFragment() {
        return getEditFilterFragment(PARAM_NONE);
    }

    private SettingsFragment getEditFilterFragment(long param) {
        if(editFilterFragment == null) {
            editFilterFragment = new SettingsEditFilterFragment();
            editFilterFragment.setHasNavigation(false);
        }
        if(param != PARAM_NONE) {
            editFilterFragment.setParam(param);
            editFilterFragment.setTitle(getString(R.string.title_edit_filter));
        } else {
            editFilterFragment.setParam(INVALID_ID);
            editFilterFragment.setTitle(getString(R.string.title_new_filter));
        }
        return editFilterFragment;
    }

    private SettingsFragment getCacheFragment() {
        if(cacheFragment == null) {
            cacheFragment = new SettingsCacheFragment();
            cacheFragment.setTitle(getString(R.string.title_cache_settings));
            cacheFragment.setHasNavigation(true);
        }
        return cacheFragment;
    }
}
