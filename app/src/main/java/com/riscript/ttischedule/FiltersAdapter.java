package com.riscript.ttischedule;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.Filters;
import com.riscript.ttischedule.draghelpers.ItemTouchHelperAdapter;

import java.util.ArrayList;
import java.util.Collection;

import static com.riscript.ttischedule.SettingsFragment.PARAM_NONE;

public class FiltersAdapter
        extends RecyclerView.Adapter<FiltersAdapter.ViewHolder>
        implements ItemTouchHelperAdapter, Filters.DataSetChangeListener
{
    interface OnClickListener {
        void onFilterClicked(Database.Filter filter);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout layout;
        public final TextView group;
        public final TextView teacher;
        public final TextView room;

        private Database.Filter boundFilter;

        ViewHolder(View view) {
            super(view);

            boundFilter = null;

            layout = (LinearLayout)view;
            group = (TextView)layout.findViewById(R.id.sfi_group);
            teacher = (TextView)layout.findViewById(R.id.sfi_teacher);
            room = (TextView)layout.findViewById(R.id.sfi_room);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener != null)
                        onClickListener.onFilterClicked(boundFilter);
                }
            });
        }

        public void bind(String message) {
            boundFilter = null;
            bind(group, message);
            bind(teacher, null);
            bind(room, null);
        }

        public void bind(Database.Filter filter) {
            boundFilter = filter;
            bind(group, filter.getGroup());
            bind(teacher, filter.getTeacher());
            bind(room, filter.getRoom());
        }

        private void bind(TextView textView, String value) {
            if(value != null && value.length() > 0) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(value);
            } else {
                textView.setVisibility(View.GONE);
                textView.setText(null);
            }
        }
    }

    private Filters filters;
    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
        notifyDataSetChanged();

        if(filters != null)
            filters.subscribeDataSetChange(this);
    }

    public int getFiltersCount() {
        if(filters == null)
            return 0;
        return filters.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.settings_filter_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(position < getFiltersCount()) {
            holder.bind(filters.getByOrder(position));
        } else {
            // item "add new"
            holder.bind("Add New Filter");
        }
    }

    @Override
    public int getItemCount() {
        return getFiltersCount() + 1; // last item for "add new"
    }

    @Override
    public boolean canMoveItem(int position) {
        return position < getFiltersCount();
    }

    @Override
    public boolean canDismissItem(int position) {
        return position < getFiltersCount();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(toPosition >= getFiltersCount())
            return false;
        filters.move(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        filters.removeByOrder(position);
    }

    @Override
    public void onDataSetChange() {
        notifyDataSetChanged();
    }

    @Override
    public void onMove(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
    }

    public void destroy() {
        if(filters != null)
            filters.unsubscribeDataSetChange(this);
    }
}
