package com.riscript.ttischedule;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.annotation.StyleRes;
import android.support.v7.app.AppCompatActivity;

public class PreferencesManager {

    private static volatile PreferencesManager instance;
    static PreferencesManager getInstance(Context context) {
        if(instance == null) {
            synchronized(PreferencesManager.class) {
                if(instance == null) {
                    instance = new PreferencesManager(context);
                }
            }
        }
        return instance;
    }

    private final SharedPreferences preferences;

    private final String key_theme;

    private final String theme_tti;
    private final String theme_invert;

    private PreferencesManager(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        final Resources resources = context.getResources();

        key_theme = resources.getString(R.string.pref_key_theme);

        theme_tti = resources.getString(R.string.pref_theme_tti);
        theme_invert = resources.getString(R.string.pref_theme_invert);
    }

    private String getThemeName() {
        return preferences.getString(key_theme, theme_tti);
    }

    @StyleRes
    public int getTheme() {
        String themeName = getThemeName();
        if(themeName.compareTo(theme_tti) == 0) {
            return R.style.AppThemeTTI;
        } else if(themeName.compareTo(theme_invert) == 0) {
            return R.style.AppThemeInvert;
        }
        return R.style.AppThemeTTI;
    }

    @StyleRes
    public int getThemeNoActionBar() {
        String themeName = getThemeName();
        if(themeName.compareTo(theme_tti) == 0) {
            return R.style.AppThemeTTI_NoActionBar;
        } else if(themeName.compareTo(theme_invert) == 0) {
            return R.style.AppThemeInvert_NoActionBar;
        }
        return R.style.AppThemeTTI_NoActionBar;
    }

    public void applyTheme(Activity activity, boolean actionBar) {
        activity.setTheme(actionBar ? getTheme() : getThemeNoActionBar());
    }
}
