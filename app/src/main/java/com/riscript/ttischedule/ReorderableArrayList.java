package com.riscript.ttischedule;

import java.util.ArrayList;
import java.util.Collection;

public abstract class ReorderableArrayList<T> extends ArrayList<T> {
    public ReorderableArrayList() {
        super();
    }

    public ReorderableArrayList(int initialCapacity) {
        super(initialCapacity);
    }

    public ReorderableArrayList(Collection<? extends T> c) {
        super(c);
    }

    public void move(int fromPosition, int toPosition) {
        if(fromPosition == toPosition)
            return;

        int delta = 1;
        if(toPosition < fromPosition)
            delta = -1;

        T moving = get(fromPosition);
        for(int position = fromPosition; position != toPosition; position += delta) {
            final T e = get(position + delta);
            set(position, e);
            onPositionChanged(e, position);
        }
        set(toPosition, moving);
        onPositionChanged(moving, toPosition);
    }

    public abstract void onPositionChanged(T element, int newPosition);
}
