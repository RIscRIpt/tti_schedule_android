package com.riscript.ttischedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DayScrollerAdapter extends DateRecyclerViewAdapter<DayScrollerAdapter.ViewHolder> {

     static class ViewHolder extends RecyclerView.ViewHolder {
        final LinearLayout weekdayBtn;
        final TextView date;
        final TextView dow;

        ViewHolder(LinearLayout weekdayBtn, TextView date, TextView dow) {
            super(weekdayBtn);

            this.weekdayBtn = weekdayBtn;
            this.date = date;
            this.dow = dow;
        }
    }

    private final String[] dow2str_en;

    private final SimpleDateFormat datefmt_dayMonth;

    public DayScrollerAdapter(Context ctx, long baseDayMillis) {
        super(ctx, baseDayMillis);

        dow2str_en = ctx.getResources().getStringArray(R.array.dow2str);

        datefmt_dayMonth = new SimpleDateFormat(ctx.getResources().getString(R.string.datefmt_day_month), Locale.US);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout btn = (LinearLayout)LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.ds_weekday_button, parent, false);

        TextView date = (TextView)btn.findViewById(R.id.ds_wd_btn_date);
        TextView dow = (TextView)btn.findViewById(R.id.ds_wd_btn_dow);

        return new ViewHolder(btn, date, dow);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Calendar day) {
        holder.date.setText(datefmt_dayMonth.format(day.getTime()));

        holder.dow.setText(dow2str_en[day.get(Calendar.DAY_OF_WEEK) - 1]);
        if((day.get(Calendar.DAY_OF_YEAR) & 1) == 0) {
            holder.dow.setBackgroundColor(colorEven.data);
            holder.date.setBackgroundColor(colorEven.data);
        } else {
            holder.dow.setBackgroundColor(colorOdd.data);
            holder.date.setBackgroundColor(colorOdd.data);
        }
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
}
