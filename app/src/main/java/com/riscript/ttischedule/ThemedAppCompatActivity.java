package com.riscript.ttischedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.app.AppCompatActivity;

public class ThemedAppCompatActivity extends AppCompatActivity {
    private int themeId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferencesManager.getInstance(getApplicationContext()).
                applyTheme(this, false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final PreferencesManager pm = PreferencesManager.getInstance(getApplicationContext());
        if(themeId != pm.getThemeNoActionBar()) {
            pm.applyTheme(this, false);
            recreate();
        }
    }

    @Override
    public void setTheme(@StyleRes int resid) {
        super.setTheme(resid);
        themeId = resid;
    }
}
