package com.riscript.ttischedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.DiskDbContract;
import com.riscript.ttischedule.database.Filters;
import com.riscript.ttischedule.draghelpers.ItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.Collection;

public class SettingsFiltersFragment
        extends SettingsFragment
        implements ScheduleProvider.ResponseFiltersListener, FiltersAdapter.OnClickListener, ScheduleProvider.ClearedListener {
    boolean viewCreated;
    FiltersAdapter filtersAdapter;
    private RecyclerView recyclerView;
    private ItemTouchHelper itemTouchHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.initialize();

        ScheduleProvider.getInstance()
                .subscribeClearedListener(this)
                .requestFilters(this);

        viewCreated = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(container == null)
            return null;

        if(viewCreated && recyclerView != null)
            return recyclerView;

        recyclerView = (RecyclerView)inflater.inflate(R.layout.settings_filters, container, false);

        recyclerView.setVisibility(View.GONE);
        filtersAdapter = new FiltersAdapter();
        filtersAdapter.setOnClickListener(this);
        recyclerView.setAdapter(filtersAdapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(filtersAdapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        viewCreated = true;

        return recyclerView;
    }

    @Override
    public void onPause() {
        super.onPause();

        ScheduleProvider.getInstance()
                .cancelAllRequests(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        filtersAdapter.destroy();

        ScheduleProvider.getInstance()
                .unsubscribeClearedListener(this);
    }

    @Override
    public void onFiltersRetrieved(Filters filters) {
        filtersAdapter.setFilters(filters);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFilterClicked(Database.Filter filter) {
        if(filter != null) {
            settingsActivity.navigateTo(SettingsActivity.PAGE_EDIT_FILTER, filter.getId());
        } else {
            settingsActivity.navigateTo(SettingsActivity.PAGE_EDIT_FILTER, PARAM_NONE);
        }
    }

    @Override
    public void onCleared() {
        filtersAdapter.onDataSetChange();
    }
}
