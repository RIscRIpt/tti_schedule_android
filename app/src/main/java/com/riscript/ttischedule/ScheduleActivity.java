package com.riscript.ttischedule;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.riscript.ttischedule.database.Filters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class ScheduleActivity
        extends ThemedAppCompatActivity
        implements ScheduleProvider.ErrorListener,
        ScheduleProvider.ResponseFiltersListener,
        ScheduleProvider.InitializedListener,
        SchedulePageAdapter.PageCountChangeListener, ScheduleProvider.ClearedListener {
    private static final int PERMISSIONS_REQUEST_WRITE_SCREENSHOT = 1;

    private enum Status {
        NONE,
        LOADING,
        NO_FILTERS,
        SHOWING_SCHEDULE,
    }

    private Status prevStatus;

    private Toolbar toolbar;
    private SchedulePageAdapter pageAdapter;
    private ViewPager pager;

    private ScheduleScrollSyncer scrollSyncer;

    protected RecyclerView schedule;
    protected RecyclerView daysScroller;

    protected ImageButton btnSyncViews;
    protected View messageContainer;
    protected TextView message;
    protected ProgressBar progressBar;
    protected Button btnAddFilter;
    protected View scheduleContainer;

    protected MenuItem menuItem_sync;
    protected MenuItem menuItem_editFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScheduleProvider.getInstance()
                .subscribeErrorListener(this)
                .subscribeClearedListener(this)
                .initialize(this, getApplicationContext());

        prevStatus = Status.NONE;

        setContentView(R.layout.activity_schedule);

        //speedup options menu creation
        openOptionsMenu();

        toolbar = (Toolbar)findViewById(R.id.schedule_toolbar);
        setSupportActionBar(toolbar);

        btnSyncViews = (ImageButton)findViewById(R.id.btn_syncviews);
        btnSyncViews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncViews();
            }
        });

        daysScroller = (RecyclerView)findViewById(R.id.days_scroller);
        daysScroller.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));
        daysScroller.setAdapter(new DayScrollerAdapter(this, -1));

        scrollSyncer = new ScheduleScrollSyncer();
        scrollSyncer.setDays(daysScroller);

        pageAdapter = new SchedulePageAdapter(getSupportFragmentManager());
        pageAdapter.setPageCountChangeListener(this);
        pager = (ViewPager)findViewById(R.id.schedule_pages);
        pager.setAdapter(pageAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                onSchedulePageChanged(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });

        messageContainer = findViewById(R.id.message_container);
        message = (TextView)messageContainer.findViewById(R.id.message);
        progressBar = (ProgressBar)messageContainer.findViewById(R.id.loading_pb);
        btnAddFilter = (Button)messageContainer.findViewById(R.id.btn_msg_add_filter);

        btnAddFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleItemSelect(R.id.action_new_filter);
            }
        });

        scheduleContainer = findViewById(R.id.schedule_container);

        // get menu item views in onCreateOptionsMenu
        invalidateOptionsMenu();

        setStatus(Status.LOADING);

        ScheduleProvider.getInstance()
                .requestFilters(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(pageAdapter.getCount() > 0)
            onSchedulePageChanged(pager.getCurrentItem());
    }

    @Override
    public void onPause() {
        super.onPause();

        ScheduleProvider.getInstance()
                .cancelAllRequests(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ScheduleProvider.getInstance()
                .unsubscribeClearedListener(this)
                .unsubscribeErrorListener(this);

        pageAdapter.destroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_schedule, menu);

        menuItem_sync = menu.findItem(R.id.action_sync);
        menuItem_editFilter = menu.findItem(R.id.action_edit_filter);

        updateStatusForMenuItems();

        return true;
    }

    @Override
    public void onInitialized() {

    }

    private void syncViews() {
        final DateLayoutManager lm = (DateLayoutManager)daysScroller.getLayoutManager();
        final int firstVisible = lm.findFirstCompletelyVisibleItemPosition();

        lm.scrollToPositionWithOffset(firstVisible, 0);
    }

    private void onSchedulePageChanged(int position) {
        ScheduleRecyclerView page = (ScheduleRecyclerView)pageAdapter.getItem(position);
        scrollSyncer.setList(page.getRecyclerView());
        toolbar.setTitle(pageAdapter.getFilter(position).getTitle());
    }

    void setStatus(Status status) {
        if(status == prevStatus)
            return;

        switch(status) {
            case LOADING:
                toolbar.setTitle(
                        getResources().getString(R.string.app_name)
                );
                message.setText(
                        getResources().getString(R.string.loading)
                );

                // making it invisible, so getItem in page adapter would be called
                scheduleContainer.setVisibility(View.VISIBLE);

                message.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                btnAddFilter.setVisibility(View.GONE);
                messageContainer.setVisibility(View.VISIBLE);
                break;
            case NO_FILTERS:
                toolbar.setTitle(
                        getResources().getString(R.string.app_name)
                );
                message.setText(
                        getResources().getString(R.string.no_filters_offer_create)
                );

                scheduleContainer.setVisibility(View.GONE);
                message.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                btnAddFilter.setVisibility(View.VISIBLE);
                messageContainer.setVisibility(View.VISIBLE);
                break;
            case SHOWING_SCHEDULE:
                // toolbar title should be set by onSchedulePageChanged.
                // message is hidden.

                messageContainer.setVisibility(View.GONE);
                scheduleContainer.setVisibility(View.VISIBLE);
                break;
        }

        prevStatus = status;

        updateStatusForMenuItems();
    }

    private void updateStatusForMenuItems() {
        switch(prevStatus) {
            case LOADING:
                if(menuItem_sync != null) menuItem_sync.setVisible(false);
                if(menuItem_editFilter != null) menuItem_editFilter.setVisible(false);
                break;
            case NO_FILTERS:
                if(menuItem_sync != null) menuItem_sync.setVisible(false);
                if(menuItem_editFilter != null) menuItem_editFilter.setVisible(false);
                break;
            case SHOWING_SCHEDULE:
                if(menuItem_sync != null) menuItem_sync.setVisible(true);
                if(menuItem_editFilter != null) menuItem_editFilter.setVisible(true);
                break;
        }
    }

    private boolean handleItemSelect(int id) {
        Intent intent = null;
        switch(id) {
            case R.id.action_sync:
                pageAdapter.refreshFilter(pager.getCurrentItem());
                break;
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE, SettingsActivity.PAGE_FILTERS);
                break;
            case R.id.action_edit_filter:
                intent = new Intent(this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE, SettingsActivity.PAGE_EDIT_FILTER);
                intent.putExtra(SettingsFragment.ARGUMENT_PARAM,
                        pageAdapter.getFilter(pager.getCurrentItem()).getId());
                break;
            case R.id.action_new_filter:
                intent = new Intent(this, SettingsActivity.class);
                intent.putExtra(SettingsActivity.PAGE, SettingsActivity.PAGE_NEW_FILTER);
                break;
            case R.id.action_share:
                shareScreen();
                break;
            case R.id.action_change_theme:
                intent = new Intent(this, PreferencesActivity.class);
                intent.putExtra(PreferenceActivity.EXTRA_SHOW_FRAGMENT, PreferencesActivity.GeneralPreferenceFragment.class.getName());
                intent.putExtra(PreferenceActivity.EXTRA_NO_HEADERS, true);
                break;
            default:
                return false;
        }
        if(intent != null) {
            startActivity(intent);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!handleItemSelect(item.getItemId())) {
            return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCleared() {
        pageAdapter.onDataSetChange();
    }

    @Override
    public void onFiltersRetrieved(Filters filters) {
        pageAdapter.setFilters(filters);
    }

    @Override
    public void onPageCountChanged(int newCount) {
        if(newCount > 0) {
            setStatus(Status.SHOWING_SCHEDULE);
            onSchedulePageChanged(0);
        } else {
            setStatus(Status.NO_FILTERS);
        }
    }


    // http://stackoverflow.com/a/30212385/1901561
    void shareScreen() {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content).getRootView();

        // Take screenshot
        rootView.setDrawingCacheEnabled(true);
        Bitmap b = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.setDrawingCacheEnabled(false);

        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(permission == PERMISSION_GRANTED) {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.filename_screenshot));
            try {
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.PNG, 100, fout);
                fout.flush();
                fout.close();
            } catch(IOException e) {
                onError(e.getMessage());
                return;
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            startActivity(Intent.createChooser(intent, getString(R.string.share_intent_title)));
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    PERMISSIONS_REQUEST_WRITE_SCREENSHOT
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch(requestCode) {
            case PERMISSIONS_REQUEST_WRITE_SCREENSHOT:
                if(grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    shareScreen();
                } else {
                    onError(getString(R.string.cannot_share_screenshot));
                }
                break;
        }
    }
}
