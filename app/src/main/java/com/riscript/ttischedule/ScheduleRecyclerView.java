package com.riscript.ttischedule;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.riscript.ttischedule.database.Database;
import com.riscript.ttischedule.database.DiskDbContract;
import com.riscript.ttischedule.database.Schedule;

import java.util.Calendar;

//https://developer.android.com/reference/android/support/v4/app/FragmentPagerAdapter.html
//creating instance of static class

public class ScheduleRecyclerView
        extends Fragment
        implements ScheduleProvider.ResponseScheduleListener
{
    public static final String ARG_FOCUSED_DAY = "focused_day";
    public static final String ARG_DATA_SOURCE = "data_source";

    private FrameLayout layout;
    private View loadingView;
    private RecyclerView recyclerView;
    private long baseDayMillis;
    private long filterId;

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        baseDayMillis = args.getLong(ARG_FOCUSED_DAY, -1);

        filterId = args.getLong(ARG_DATA_SOURCE);

        reloadSchedule();

        layout = null;
        recyclerView = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = (FrameLayout)inflater
                .inflate(R.layout.schedule_scrollview, container, false);

        loadingView = layout.findViewById(R.id.schedule_loading);
        recyclerView = (RecyclerView)layout.findViewById(R.id.schedule_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        if(recyclerView.getAdapter() == null)
            displayLoading();

        return layout;
    }

    @Override
    public void onScheduleReceived(Schedule schedule) {
        if(schedule.getFilter().getId() != filterId)
            return;

        // ignore response if fragment destroyed
        if(getContext() == null)
            return;

        displaySchedule();

        // bug? need to save positions
        // https://issuetracker.google.com/issues/37051107
        final DateLayoutManager lm = (DateLayoutManager)recyclerView.getLayoutManager();
        int firstVisiblePos = lm.findFirstVisibleItemPosition();
        recyclerView.setAdapter(new ScheduleAdapter(this, schedule, baseDayMillis));
        if(firstVisiblePos != -1)
            lm.scrollToPositionWithOffset(firstVisiblePos, 0);
    }

    public void displayLoading() {
        if(recyclerView != null)
            recyclerView.setVisibility(View.GONE);
        if(loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    public void displaySchedule() {
        if(loadingView != null)
            loadingView.setVisibility(View.GONE);
        if(recyclerView != null)
            recyclerView.setVisibility(View.VISIBLE);
    }

    public void reloadSchedule() {
        ScheduleProvider.getInstance()
                .requestSchedule(this, filterId);

        displayLoading();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, R.integer.scm_set_reminder_id, Menu.NONE, R.string.scm_set_reminder);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch(item.getItemId()) {
            case R.integer.scm_set_reminder_id:
                Database.Class aClass = ((Database.Class)(((ExpandedListView)(info.targetView.getParent())).getAdapter().getItem(info.position)));
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", aClass.time.getTimeInMillis());
                intent.putExtra("allDay", false);
                intent.putExtra("endTime", aClass.time.getTimeInMillis()+90*60*1000);
                intent.putExtra("title", aClass.name);
                startActivity(intent);
                return true;
        }
        return super.onContextItemSelected(item);
    }
}
