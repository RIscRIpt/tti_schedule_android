package com.riscript.ttischedule;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class DateLayoutManager extends LinearLayoutManager {
    public static final int ITEM_COUNT = Integer.MAX_VALUE;
    public static final int BASE_POSITION = Integer.MAX_VALUE / 2;

    public DateLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        scrollToPosition(BASE_POSITION);
    }
}
